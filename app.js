App({
	onLaunch: function(){
		console.log('=================onLaunch==================');
	},
	onShow: function(){
		console.log('=================onShow==================');
	},
	onHide: function(){

	},
	util: require('static/js/utils/util.js'),
	Lang: require('static/js/utils/lang.js'),
	WxParse: require('./library/wxParse/wxParse.js'),
	ext: {
		"siteInfo": {
			"uniacid": "1",
			"siteroot": "https://qdb.qdban.com/app/wxapp.php",
			"sitebase": "https://qdb.qdban.com/app",
			"module": "we7_wmall_manager",
			"lang": "zh-cn",
			"version": "3.8"
		}
	}
});