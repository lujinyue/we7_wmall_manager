var wxTimer = function (initObj){
	initObj = initObj || {};
	this.endTime = initObj.endTime;	//开始时间
	this.interval = initObj.interval || 0;				//间隔时间
	this.complete = initObj.complete;					//结束任务
	this.intervalFn = initObj.intervalFn;				//间隔任务
	this.name = initObj.name;							//当前计时器在计时器数组对象中的名字
	this.issplit = initObj.issplit ? initObj.issplit : false;

	this.intervarID;									//计时ID
}

wxTimer.prototype = {
	//开始
	start:function(self){
		var that = this;
		this.systemEndTime = new Date(this.endTime).getTime();//1970年1月1日的00：00：00的字符串日期
		//开始倒计时
		var count = 0;//这个count在这里应该是表示s数，js中获得时间是ms，所以下面*1000都换成ms
		function begin(){
			var sys_second = (that.systemEndTime - new Date().getTime()) / 1000;
			var wxTimerDay = Math.floor((sys_second / 3600) / 24);
			var wxTimerHour = Math.floor((sys_second / 3600) % 24);
			var wxTimerMinute = Math.floor((sys_second / 60) % 60);
			var wxTimerSecond = Math.floor(sys_second % 60);
			wxTimerDay = wxTimerDay < 10 ? "0" + wxTimerDay : wxTimerDay;
			wxTimerHour = wxTimerHour < 10 ? "0" + wxTimerHour : wxTimerHour;
			wxTimerMinute = wxTimerMinute < 10 ? "0" + wxTimerMinute : wxTimerMinute;
			wxTimerSecond = wxTimerSecond < 10 ? "0" + wxTimerSecond : wxTimerSecond;

			var wxTimerList = self.data.wxTimerList || [];
			//更新计时器数组
			if(!that.issplit) {
				wxTimerList[that.name] = {
					wxTimerSecond:wxTimerSecond,
					wxTimerDay:wxTimerDay,
					wxTimerHour:wxTimerHour,
					wxTimerMinute:wxTimerMinute,
					wxTimerSecond:wxTimerSecond
				}
			} else {
				wxTimerDay = String(wxTimerDay).split('');
				wxTimerHour = String(wxTimerHour).split('');
				wxTimerMinute = String(wxTimerMinute).split('');
				wxTimerSecond = String(wxTimerSecond).split('');
				wxTimerList[that.name] = {
					wxTimerSecond:wxTimerSecond,
					wxTimerDay:wxTimerDay,
					wxTimerHour:wxTimerHour,
					wxTimerMinute:wxTimerMinute,
					wxTimerSecond:wxTimerSecond
				}
			}
			self.setData({
				wxTimerSecond:wxTimerSecond,
				wxTimerDay:wxTimerDay,
				wxTimerHour:wxTimerHour,
				wxTimerMinute:wxTimerMinute,
				wxTimerSecond:wxTimerSecond,
				wxTimerList:wxTimerList
			});
			//时间间隔执行函数
			if( 0 == (count-1) % that.interval && that.intervalFn){
				that.intervalFn();
			}
			//结束执行函数
			if(sys_second <= 0){
				if(that.complete){
					that.complete();
				}
				that.stop();
			}
		}
		begin();
		this.intervarID = setInterval(begin,1000);
	},
	//结束
	stop:function(){
		clearInterval(this.intervarID);
	}
}

module.exports = wxTimer;
