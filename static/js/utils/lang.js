var LangArray = {
	//简体中文
	'zh-cn': {
		dollarSign: '￥',
		dollarSignCn: '元'
	},

	//欧洲英文
	'europe': {
		dollarSign: '€',
		dollarSignCn: '欧元'
	},

	//美/英
	'en': {
		dollarSign: '$',
		dollarSignCn: '美元'
	},

	//港元
	'HKD': {
		dollarSign: 'HKD',
		dollarSignCn: '港元'
	},

	//卢布
	'RUB': {
		dollarSign: '₽',
		dollarSignCn: '卢布'
	},

	//马来西亚
	'RM': {
		dollarSign: 'RM',
		dollarSignCn: '马来西亚林吉特'
	},

	//菲律宾比索
	'PHP': {
		dollarSign: '₱',
		dollarSignCn: '菲律宾比索'
	},

	'THB': {
		dollarSign: '฿',
		dollarSignCn: '泰铢'
	},
};

var DollarType  ='zh-cn';
var LocalDollarType = wx.getStorageSync('DollarType');
if(LocalDollarType) {
	DollarType = LocalDollarType;
}
var Lang = LangArray[DollarType];
module.exports = Lang;