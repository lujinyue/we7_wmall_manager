// pages/statcenter/statistic.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,

	},

	onLoad: function (options) {
		var that = this;
		app.util.request({
			url: 'manage/statcenter/statistic',
			data: {days: options.days || 0},
			success: function (res){
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				that.setData(result.message);
				// console.log(result.message);
			}
		});
	},

	onChangeStatus: function(e){
		var days = e.currentTarget.dataset.days;
		this.setData({days: days});
		this.onLoad({days:days});
	},

	changeStartTime: function(e){
		this.setData({ startTime: e.detail.value });
	},

	changeEndTime: function(e){
		this.setData({ endTime: e.detail.value });
	},

	onSubmit: function(e){
		var that = this;
		var values = e.detail.value;
		if (!values.start) {
			app.util.toast('开始时间不能为空', '', 1000);
			return false;
		}
		if (!values.end) {
			app.util.toast('结束时间不能为空', '', 1000);
			return false;
		}
		var params = {
			start: values.start,
			end: values.end,
			days: '-1',
		}
		app.util.request({
			url: 'manage/statcenter/statistic',
			data: params,
			method: 'POST',
			success: function (result) {
				if (result.data.message.errno == 0) {
					that.setData(result.data.message.message);
				} else {
					app.util.toast(result.data.message.message);
				}
			}
		});
	},
	/**
	* 生命周期函数--监听页面初次渲染完成
	*/
	onReady: function () {

	},

	/**
	* 生命周期函数--监听页面显示
	*/
	onShow: function () {

	},

	/**
	* 生命周期函数--监听页面隐藏
	*/
	onHide: function () {

	},

	/**
	* 生命周期函数--监听页面卸载
	*/
	onUnload: function () {

	},

	/**
	* 页面相关事件处理函数--监听用户下拉动作
	*/
	onPullDownRefresh: function () {

	},

	/**
	* 页面上拉触底事件的处理函数
	*/
	onReachBottom: function () {

	},

	/**
	* 用户点击右上角分享
	*/
	onShareAppMessage: function () {

	}
})