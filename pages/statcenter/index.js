// pages/statcenter/index.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,

	},

	onLoad: function () {
		var that = this;
		app.util.request({
			url: 'manage/statcenter/index',
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				that.setData(result.message)
			}
		});
	},

	onPullDownRefresh: function () {
		this.onLoad();
		wx.stopPullDownRefresh();
	}
})