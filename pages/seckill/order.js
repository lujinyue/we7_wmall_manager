var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		status: 0,
		orders: {
			page: 1,
			psize: 15,
			empty: false,
			loaded: false,
			data: []
		},
		showPopup: false
	},

	onLoad: function(options) {
		var that = this;
		that.onPullDownRefresh();
		return;
	},

	onChangeStatus: function(e) {
		var that = this;
		var status = e.currentTarget.dataset.status;
		if(status == that.data.status) {
			return false;
		}
		that.setData({
			status: status,
			orders: {
				page: 1,
				psize: 15,
				empty: false,
				loaded: false,
				data: []
			}
		});
		that.onReachBottom();
	},

	onShowPopup: function(e) {
		var that = this;
		var orderId = e.currentTarget.dataset.id;
		if(orderId) {
			that.setData({
				orderId: orderId
			})
		}
		that.setData({
			showPopup: !that.data.showPopup
		})
	},

	onReceive: function(e) {
		var that = this;
		var code = e.detail.value.code;
		if(!code) {
			app.util.toast('请输入兑换码');
			return false;
		}
		app.util.request({
			url: 'manage/seckill/order/status',
			data: {
				id: that.data.orderId,
				code: code,
				type: 'status',
				formid: e.detail.formId
			},
			success: function(res) {
				var result = res.data.message;
				app.util.toast(result.message);
				if(result.errno) {
					return;
				}
				that.setData({
					showPopup: false
				})
				that.onLoad();
			}
		})

	},

	onShowGoods: function(e) {
		var index = e.currentTarget.dataset.index;
		this.data.orders.data[index]['showGoods'] = !this.data.orders.data[index]['showGoods'];
		this.setData({'orders.data': this.data.orders.data});
	},

	onPullDownRefresh: function () {
		var that = this;
		that.setData({
			status: 0,
			orders: {
				page: 1,
				psize: 15,
				empty: false,
				loaded: false,
				data: []
			}
		});
		that.onReachBottom();
		wx.stopPullDownRefresh();
	},

	onReachBottom: function () {
		var that = this;
		if(that.data.orders.loaded) {
			return false;
		}
		app.util.request({
			url: 'manage/seckill/order',
			data: {
				status: that.data.status,
				page: that.data.orders.page,
				psize: that.data.orders.psize,
			},
			success: function(res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return;
				}
				result = result.message;
				var orders = that.data.orders.data.concat(result.orders);
				that.data.orders.data = orders;
				that.data.orders.page++;
				if(!orders.length) {
					that.data.orders.empty = true;
				}
				if(result.orders.length < that.data.orders.psize) {
					that.data.orders.loaded = true;
				}
				that.setData({
					orders: that.data.orders,
				})
			}
		})
	},

	onJsEvent: function(e) {
		app.util.jsEvent(e);
	},
})