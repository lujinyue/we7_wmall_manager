var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		submit: true,
		channel: 'weixin'
	},

	onLoad: function (options) {
		var that = this;
		app.util.request({
			url: 'manage/finance/getcash',
			success: function(res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return;
				}
				that.setData({
					account: result.message.account,
					config: result.message.config,
					submit: false
				});
			}
		})
	},

	onSubmit: function(e) {
		var that = this;
		var fee = parseFloat(e.detail.value.fee);
		var account = that.data.account;
		if(that.data.submit) {
			return false;
		}
		if(isNaN(fee)) {
			app.util.toast('提现金额有误');
			return false;
		}
		if(fee > account.amount) {
			app.util.toast('提现金额不能大于账户可用余额');
			return false;
		}
		if(fee < account.fee_limit) {
			app.util.toast('提现金额不能小于' + account.fee_limit + that.data.Lang.dollarSignCn);
			return false;
		}
		var rule_fee = (fee * account.fee_rate/100).toFixed(2);
		rule_fee = Math.max(rule_fee, account.fee_min);
		if(account.fee_max > 0) {
			rule_fee = Math.min(rule_fee, account.fee_max);
		}
		rule_fee = parseFloat(rule_fee);
		var final_fee = (fee - rule_fee).toFixed(2);
		var tips = "提现金额" + fee + that.data.Lang.dollarSignCn + ", 手续费" + rule_fee + that.data.Lang.dollarSignCn + ",实际到账" + final_fee + that.data.Lang.dollarSignCn + ", 确定提现吗";
		wx.showModal({
			content: tips,
			success: function(res) {
				if(res.confirm) {
					that.setData({submit: true});
					app.util.request({
						url: 'manage/finance/getcash/getcash',
						methods: 'POST',
						data: {
							fee: fee,
							formid: e.detail.formId,
							channel: that.data.channel
						},
						success: function(res) {
							var result = res.data.message;
							if(result.errno) {
								app.util.toast(result.message);
								that.setData({submit: false});
								return;
							}
							app.util.toast(result.message, '/pages/shop/setting', 3000)
							return;
						}
					})
				} else if(res.cancel) {

				}
			}
		})
	},

	onChangeType: function(type) {
		var channel = type.currentTarget.dataset.channel;
		this.setData({
			channel: channel
		});
	},

})