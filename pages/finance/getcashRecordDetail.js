// pages/finance/getcashDetail.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		
	},

	onLoad: function (options) {
		var that = this;
		if(!options.id) {
			app.util.toast('参数错误');
			return false;
		}
		app.util.request({
			url: 'manage/finance/getcashRecord/detail',
			data: {
				id: options.id
			},
			success: function(res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return;
				}
				that.setData(result.message)
			}
		})
	}
})