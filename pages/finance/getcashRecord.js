// pages/finance/getcashRecord.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		status: 0,
		showLoading: false,
		record: {
			page: 1,
			psize: 15,
			empty: false,
			loaded: false,
			data: []
		}
	},

	/**
	* 生命周期函数--监听页面加载
	*/
	onLoad: function(options) {
		var that = this;
		that.onReachBottom();
		return;
	},

	onChangeType: function(e) {
		var that = this;
		var status = e.currentTarget.dataset.type;
		if(status == that.data.status) {
			return false;
		}
		that.setData({
			status: status,
			record: {
				page: 1,
				psize: 15,
				empty: false,
				loaded: false,
				data: []
			}
		});
		that.onReachBottom();
	},

	/**
	* 页面上拉触底事件的处理函数
	*/
	onReachBottom: function () {
		var that = this;
		if(that.data.record.loaded) {
			return false;
		}
		that.setData({showLoading: true});
		app.util.request({
			url: 'manage/finance/getcashRecord',
			data: {
				status: that.data.status,
				page: that.data.record.page,
				psize: that.data.record.psize,
			},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				result = result.message;
				var record = that.data.record.data.concat(result.record);
				that.data.record.data = record;
				that.data.record.page++;
				if(!record.length) {
					that.data.record.empty = true;
				}
				if(result.record.length < that.data.record.psize) {
					that.data.record.loaded = true;
				}
				that.setData({
					record: that.data.record,
					showLoading: false
				})
			}
		})
	},

	onJsEvent: function(e) {
		app.util.jsEvent(e);
	},
	/**
	* 用户点击右上角分享
	*/
	onShareAppMessage: function () {

	}
})