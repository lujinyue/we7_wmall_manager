var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		trade_type: 0,
		showLoading: false,
		records: {
			page: 1,
			psize: 15,
			empty: false,
			loaded: false,
			data: []
		}
	},

	onLoad: function(options) {
		var that = this;
		that.onReachBottom();
		return;
	},

	onChangeType: function(e) {
		var that = this;
		var trade_type = e.currentTarget.dataset.type;
		if(trade_type == that.data.trade_type) {
			return false;
		}
		that.setData({
			trade_type: trade_type,
			records: {
				page: 1,
				psize: 15,
				empty: false,
				loaded: false,
				data: []
			}
		});
		that.onReachBottom();
	},

	onPullDownRefresh: function () {
		var that = this;
		that.setData({
			trade_type: 0,
			records: {
				page: 1,
				psize: 15,
				empty: false,
				loaded: false,
				data: []
			}
		});
		that.onLoad();
		wx.stopPullDownRefresh();
	},

	onReachBottom: function () {
		var that = this;
		if(that.data.records.loaded) {
			return false;
		}
		that.setData({showLoading: true});
		app.util.request({
			url: 'manage/finance/current',
			data: {
				trade_type: that.data.trade_type,
				page: that.data.records.page,
				psize: that.data.records.psize,
			},
			success: function(res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return;
				}
				result = result.message;
				var records = that.data.records.data.concat(result.records);
				that.data.records.data = records;
				that.data.records.page++;
				if(!records.length) {
					that.data.records.empty = true;
				}
				if(result.records.length < that.data.records.psize) {
					that.data.records.loaded = true;
				}
				that.setData({
					records: that.data.records,
					showLoading: false
				})
			}
		})
	},

	onJsEvent: function(e) {
		app.util.jsEvent(e);
	},
})