 // pages/order/index.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		status: 1,
		refresh: 0,
		showLoading: false,
		orders: {
			page: 1,
			psize: 10,
			loaded: false,
			//empty: 0,
			data: []
		},
		showGoods: false,
		yinsihao: {
			status: false,
			secret_mobile: '',
			extension: ''
		}
	},

	onJsEvent: function (e) {
		app.util.jsEvent(e);
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		var that = this;
		if(options && options.sid) {
			app.util.setSid(options.sid);
		}
		app.util.uploadOpenid();
		that.onReachBottom();
		return;
	},

	onCallCustomer: function(e) {
		var that = this;
		var order = e.currentTarget.dataset.order;
		if(order && order.data && order.data.yinsihao_status == 1) {
			app.util.request({
				url: 'yinsihao/yinsihao',
				data: {
					order_id: order.id,
					type: 'member',
					ordersn: order.ordersn
				},
				success: function (res) {
					var result = res.data.message;
					if(result.errno) {
						app.util.toast(result.message, '', 1000);
						return false;
					}
					result = result.message;
					that.data.yinsihao.secret_mobile = result.data.secret_mobile;
					that.data.yinsihao.extension = result.data.extension;
					that.data.yinsihao.status = true;
					that.setData({
						yinsihao: that.data.yinsihao
					})
				}
			});
		} else {
			wx.makePhoneCall({
				phoneNumber: order.mobile
			})
		}
	},

	onCallSecretMobile: function() {
		var that =  this;
		that.onToggleYinsihaoStatus();
		wx.makePhoneCall({
			phoneNumber: that.data.yinsihao.secret_mobile
		})
	},

	onToggleYinsihaoStatus: function() {
		var that = this;
		that.setData({
			'yinsihao.status': !that.data.yinsihao.status
		})
	},

	onChangeOrderStatus: function(e) {
		var that = this;
		var dataset = e.currentTarget.dataset;
		var type = dataset.type;
		var id = dataset.id;
		var status = dataset.status;
		if(type == 'cancel' || type == 'direct_deliveryer') {
			wx.navigateTo({
				url: './op?type=' + type + '&id=' + id
			});
			return false;
		}
		if(type == 'handle' && dataset.is_reserve == 1) {
			dataset.confirm = dataset.reserve_confirm;
		}
		wx.showModal({
			title: '系统提示',
			content: dataset.confirm,
			success: function(res) {
				if(res.confirm) {
					app.util.request({
						url: 'manage/order/takeout/status',
						data: dataset,
						success: function (res) {
							var result = res.data.message;
							app.util.toast(result.message, '', 1000);
							if(!result.errno) {
								that.onPullDownRefresh();
							}
						}
					});
				} else if(res.cancel) {
				}
			}
		});
	},

	onPushOtherPlateform: function(e) {
		var type = e.currentTarget.dataset.type;
		var url = 'manage/order/takeout/push_uupaotui';
		if(type == 'shansong') {
			url = 'manage/order/takeout/push_shansong';
		} else if(type == 'dianwoda') {
			url = 'manage/order/takeout/push_dianwoda';
		} else if(type == 'ishansong') {
			url = 'manage/order/takeout/push_ishansong';
		} else if(type == 'sfexpress') {
			url = 'manage/order/takeout/push_sfexpress';
		} else if(type == 'kuaipaozhe') {
			url = 'manage/order/takeout/push_kuaipaozhe';
		} else if(type == 'fengniao') {
			url = 'manage/order/takeout/push_fengniao';
		} else if(type == 'ledisong') {
			url = 'manage/order/takeout/push_ledisong';
		}
		app.util.request({
			url: url,
			data: {
				id: e.currentTarget.dataset.id,
				push: 0
			},
			method: 'POST',
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				if(type == 'fengniao' || type == 'ledisong') {
					app.util.toast(result.message);
					return true;
				}
				wx.showModal({
					title: '系统提示',
					content: result.message.tips,
					success: function(res) {
						if(res.confirm) {
							app.util.request({
								url: url,
								data: {
									id: result.message.id,
									push: 1
								},
								method: 'POST',
								success: function (res) {
									var result = res.data.message;
									app.util.toast(result.message, '', 1000);
									return false;
								}
							});
						} else if(res.cancel) {
						}
					}
				});
			}
		});
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let order_refresh = app.util.getStorageSync('order_refresh');
		if(order_refresh) {
			app.util.removeStorageSync('order_refresh');
			this.setData({
				refresh: 1,
				status: 1,
			})
			this.onReachBottom();
		}
	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {
		this.data.refresh = 1;
		this.onReachBottom();
		wx.stopPullDownRefresh();
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		var that = this;
		if(that.data.refresh == 1) {
			that.data.orders = {
				page: 1,
				psize: 10,
				loaded: false,
				data:[]
			};
		}
		if (that.data.orders.loaded) {
			return false;
		}
		that.setData({showLoading: true});
		app.util.request({
			url: 'manage/order/takeout/list',
			data: {
				status: that.data.status,
				page: that.data.orders.page,
				psize: that.data.orders.psize
			},
			//showLoading: false,
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				var orders = that.data.orders.data.concat(result.message.orders);
				that.data.orders.data = orders;
				if (result.message.orders.length < that.data.orders.psize) {
					that.data.orders.loaded = true;
/*					if(!orders.length) {
						that.data.orders.empty = 1;
					}*/
				}
				that.data.orders.page++;
				that.data.refresh = 0;
				that.setData({
					orders: that.data.orders,
					showLoading: false,
					store: result.message.store
				});
				if(result.message.openid_wxapp_manager) {
					var clerkInfo = app.util.getStorageSync('clerkInfo');
					if(!clerkInfo.openid_wxapp_manager) {
						clerkInfo.openid_wxapp_manager = result.message.openid_wxapp_manager;
						app.util.setStorageSync('clerkInfo', clerkInfo);
					}
				}
			}
		});
	},

	onChangeStatus: function(e) {
		var that = this;
		var status = e.currentTarget.dataset.status;
		if(that.data.status != status) {
			that.data.refresh = 1;
		}
		that.setData({status: status});
		that.onReachBottom();
	},

	onShowGoods: function(e) {
		var index = e.currentTarget.dataset.index;
		this.data.orders.data[index]['showGoods'] = !this.data.orders.data[index]['showGoods'];
		this.setData({'orders.data': this.data.orders.data});
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	},

	onPullDownRefresh: function () {
		var that = this;
		that.data.refresh = 1;
		that.onReachBottom();
		wx.stopPullDownRefresh();
	}
})