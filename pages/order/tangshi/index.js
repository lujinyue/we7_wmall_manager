// pages/order/tangshilist.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		status: 1,
		refresh: 0,
		showLoading: false,
		orders: {
			page: 1,
			psize: 10,
			loaded: false,
			data: []
		},
		showGoods: false

	},

	onJsEvent: function (e) {
		app.util.jsEvent(e);
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		var that = this;
		that.onReachBottom();
		return;
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {
		this.data.refresh = 1;
		this.onReachBottom();
		wx.stopPullDownRefresh();
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		var that = this;
		if(that.data.refresh == 1) {
			that.data.orders = {
				page: 1,
				psize: 10,
				loaded: false,
				data:[]
			};
		}
		if (that.data.orders.loaded) {
			return false;
		}
		that.setData({showLoading: true});
		app.util.request({
			url: 'manage/order/tangshi/list',
			data: {
				status: that.data.status,
				page: that.data.orders.page,
				psize: that.data.orders.psize
			},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				var orders = that.data.orders.data.concat(result.message.orders);
				that.data.orders.data = orders;
				if (result.message.orders.length < that.data.orders.psize) {
					that.data.orders.loaded = true;
				}
				that.data.refresh = 0;
				that.setData({
					orders: that.data.orders,
					showLoading: false
				});
			}
		});
	},

	onChangeStatus: function(e) {
		var that = this;
		var status = e.currentTarget.dataset.status;
		if(that.data.status != status) {
			that.data.refresh = 1;
		}
		that.setData({status: status});
		that.onReachBottom();
	},

	onShowGoods: function(e) {
		var index = e.currentTarget.dataset.index;
		this.data.orders.data[index]['showGoods'] = !this.data.orders.data[index]['showGoods'];
		this.setData({'orders.data': this.data.orders.data});
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	},

	onPullDownRefresh: function () {
		var that = this;
		that.data.refresh = 1;
		that.onReachBottom();
		wx.stopPullDownRefresh();
	}
})