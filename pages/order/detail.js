var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		showStatus: false,
		yinsihao: {
			status: false,
			secret_mobile: '',
			extension: ''
		}
	},
	onLoad: function (options) {
		var that = this;
		app.util.request({
			url: 'manage/order/takeout/detail',
			data: {
				id: options.id || 433
			},
			success: function(res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return;
				}
				that.setData(result.message)
			}
		})
	},

	onCallCustomer: function() {
		var that = this;
		if(that.data.order.data && that.data.order.data.yinsihao_status == 1) {
			app.util.request({
				url: 'yinsihao/yinsihao',
				data: {
					order_id: that.data.order.id,
					type: 'member',
					ordersn: that.data.order.ordersn
				},
				success: function (res) {
					var result = res.data.message;
					if(result.errno) {
						app.util.toast(result.message, '', 1000);
						return false;
					}
					result = result.message;
					that.data.yinsihao.secret_mobile = result.data.secret_mobile;
					that.data.yinsihao.extension = result.data.extension;
					that.data.yinsihao.status = true;
					that.setData({
						yinsihao: that.data.yinsihao
					})
				}
			});
		} else {
			wx.makePhoneCall({
				phoneNumber: that.data.order.mobile
			})
		}
	},

	onCallSecretMobile: function() {
		var that =  this;
		that.onToggleYinsihaoStatus();
		wx.makePhoneCall({
			phoneNumber: that.data.yinsihao.secret_mobile
		})
	},

	onToggleYinsihaoStatus: function() {
		var that = this;
		that.setData({
			'yinsihao.status': !that.data.yinsihao.status
		})
	},

	chooseStatus: function(){
		this.setData({
			showStatus: !this.data.showStatus
		});
	},

	onJsEvent: function(e) {
		app.util.jsEvent(e);
	},

	onChangeOrderStatus: function(e) {
		var that = this;
		var dataset = e.currentTarget.dataset;
		var type = dataset.type;
		var id = dataset.id;
		var status = dataset.status;
		if(type == 'cancel' || type == 'direct_deliveryer' || type == 'reply') {
			wx.navigateTo({
				url: './op?type=' + type + '&id=' + id
			});
			return false;
		}
		if(type == 'handle' && dataset.is_reserve == 1) {
			dataset.confirm = dataset.reserve_confirm;
		}
		wx.showModal({
			title: '系统提示',
			content: dataset.confirm,
			success: function(res) {
				if(res.confirm) {
					app.util.request({
						url: 'manage/order/takeout/status',
						data: dataset,
						success: function (res) {
							var result = res.data.message;
							app.util.toast(result.message, '', 1000);
							if(!result.errno) {
								that.onLoad({id: id});
							}
						}
					});
				} else if(res.cancel) {
				}
			}
		});
	},

	onPushOtherPlateform: function(e) {
		var type = e.currentTarget.dataset.type;
		var url = 'manage/order/takeout/push_uupaotui';
		if(type == 'shansong') {
			url = 'manage/order/takeout/push_shansong';
		} else if(type == 'dianwoda') {
			url = 'manage/order/takeout/push_dianwoda';
		} else if(type == 'dada') {
			url = 'manage/order/takeout/push_dada';
		} else if(type == 'ishansong') {
			url = 'manage/order/takeout/push_ishansong';
		} else if(type == 'sfexpress') {
			url = 'manage/order/takeout/push_sfexpress';
		} else if(type == 'kuaipaozhe') {
			url = 'manage/order/takeout/push_kuaipaozhe';
		} else if(type == 'fengniao') {
			url = 'manage/order/takeout/push_fengniao';
		} else if(type == 'ledisong') {
			url = 'manage/order/takeout/push_ledisong';
		}
		app.util.request({
			url: url,
			data: {
				id: e.currentTarget.dataset.id,
				push: 0
			},
			method: 'POST',
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				if(type == 'fengniao' || type == 'ledisong') {
					app.util.toast(result.message);
					return true;
				}
				wx.showModal({
					title: '系统提示',
					content: result.message.tips,
					success: function(res) {
						if(res.confirm) {
							app.util.request({
								url: url,
								data: {
									id: result.message.id,
									push: 1
								},
								method: 'POST',
								success: function (res) {
									var result = res.data.message;
									app.util.toast(result.message, '', 1000);
									return false;
								}
							});
						} else if(res.cancel) {
						}
					}
				});
			}
		});
	},

	onPullDownRefresh: function () {
		this.onLoad({id: this.data.order.id});
		wx.stopPullDownRefresh();
	}
})