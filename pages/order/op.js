// pages/order/reason.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		checked: 0,
		data: {},
		reply: '',
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		var that = this;
		that.data.options = options;
		app.util.request({
			url: 'manage/order/takeout/op',
			data: {
				type: options.type,
				id: options.id
			},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message, '', 1000);
					return false;
				}
				that.setData({
					data: result.message.data,
					type: that.data.options.type
				});
				if(that.data.type == 'cancel') {
					var title = '选择理由';
				} else if(that.data.type == 'direct_deliveryer') {
					title = '选择配送员';
				} else if(that.data.type == 'reply') {
					title = '催单回复';
				}
				wx.setNavigationBarTitle({
					title: title
				});
			}
		});
	},

	onChoose: function(e) {
		var that = this;
		var checked = e.detail.value;
		that.setData({
			checked: checked
		});
		if(that.data.type == 'reply') {
			that.data.reply = that.data.reply + that.data.data[checked];
			that.setData({reply: that.data.reply});
		}
	},

	onInput: function(e) {
		var that = this;
		that.data.reply = that.data.reply + e.detail.value;
		that.setData({reply: that.data.reply});
	},

	onSubmit: function() {
		var that = this;
		var checked = that.data.checked;
		var type = that.data.type;
		if(type == 'direct_deliveryer') {
			var deliveryer_id = that.data.data[checked].id;
			if(!deliveryer_id) {
				app.util.toast('请选择要转单到的配送员', '', 1000);
				return false;
			}
			var params = {
				id: that.data.options.id,
				deliveryer_id: deliveryer_id
			};
			var url = 'manage/order/takeout/deliveryer';
		} else if(type == 'cancel') {
			var reason = that.data.checked;
			if(!reason) {
				app.util.toast('请先选择原因', '', 1000);
				return false;
			}
			var params = {
				id: that.data.options.id,
				reason: reason
			};
			var url = 'manage/order/takeout/cancel';
		} else if(type == 'reply') {
			var id = that.data.options.id;
			if(!that.data.reply) {
				app.util.toast('请输入回复内容', '', 1000);
				return false;
			}
			var params = {
				id: id,
				reply: that.data.reply
			};
			var url = 'manage/order/takeout/reply';
		}
		app.util.request({
			url: url,
			data: params,
			method: "POST",
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message, '', 1000);
					return false;
				}
				app.util.toast(result.message, './index', 1000);
			}
		});
	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	}
})