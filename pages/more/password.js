// pages/more/password.js
var app = getApp()
Page({
	data: {
		Lang: app.Lang,
	
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {

	},

	onSubmit1: function(e) {
		var that = this;
		var values = e.detail.value;
		if (!values.password) {
			app.util.toast('原密码不能为空');
			return false;
		}
		if (!values.newpassword) {
			app.util.toast('密码不能为空');
			return false;
		} else {
			var length = values.newpassword.length;
			if(length < 8 || length > 20) {
				app.util.toast("请输入8-20位密码");
				return false;
			}
			var reg = /[0-9]+[a-zA-Z]+[0-9a-zA-Z]*|[a-zA-Z]+[0-9]+[0-9a-zA-Z]*/;
			if(!reg.test(values.newpassword)) {
				app.util.toast("密码必须由数字和字母组合");
				return false;
			}
		}
		if (!values.repassword) {
			app.util.toast('请重复输入密码');
			return false;
		}
		if(values.newpassword != values.repassword) {
			app.util.toast('两次密码输入不一致');
			return false;
		}
		var params = {
			password: values.password,
			newpassword: values.newpassword,
			repassword: values.repassword,
			formid: e.detail.formId
		}
		app.util.request({
			url: 'manage/more/profile/password',
			data: params,
			method: 'POST',
			success: function (result) {
				if (result.data.message.errno == 0) {
					app.util.toast(result.data.message.message, '../shop/setting', 1000);
				} else {
					app.util.toast(result.data.message.message);
				}
			}
		});
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {
	
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
	
	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {
	
	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {
	
	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {
	
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
	
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {
	
	},
	oldPassword: function (e) {
		this.setData({password: e.detail.value});
	},
	newPassword: function (e) {
		this.setData({newPassword: e.detail.value});
	},
	checkPassword: function (e) {
		this.setData({rePassword: e.detail.value});
	},
	onSubmit: function () {
		var that = this;
		var params = {
			password : that.data.password,
			newpassword : that.data.newPassword,
			repassword : that.data.rePassword
		}
		app.util.request({
			url: 'manage/more/profile/password',
			data: params,
			method: 'POST',
			success: function(res) {
				if (res.data.message.errno == 0) {
					wx.redirectTo({
						url: 'profile'
					})
				}
			}
		})
	}
})