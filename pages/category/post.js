var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		weeks: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
		limit_week: {},
		now_category: {}
	},

	onLoad: function (options) {
		var that = this;
		that.data.options = options;
		app.util.request({
			url: 'manage/goods/category/post',
			data: {
				id: options.id || 0
			},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				if(!result.message.now_category.length) {
					that.data.now_category = {};
				}
				that.setData({
					categorys: result.message.categorys,
					now_category: result.message.now_category,
				});
			}
		});
	},

	onSelectParentCategory: function(event) {
		var that = this;
		var index = event.detail.value;
		that.data.parentCategory = that.data.categorys[index];
		that.setData({
			parentCategory: that.data.parentCategory,
			'now_category.parentid': that.data.parentCategory.id
		});
	},

	onSetShowtime: function(e) {
		var that = this;
		var value = e.detail.value;
		that.setData({'now_category.is_showtime': value})
	},

	onSelectWeek: function(e) {
		var that = this;
		var value = e.currentTarget.dataset.value;
		if(that.data.limit_week[value]) {
			delete that.data.limit_week[value];
		} else {
			that.data.limit_week[value] = value;
		}
		that.setData({'now_category.limit_week': that.data.limit_week});
	},

	onSelectTime: function(event) {
		var that = this;
		var value = event.detail.value;
		var type = event.currentTarget.dataset.type;
		that.data.now_category[type] = value;
		if(type == 'start_time') {
			that.setData({'now_category.start_time': value});
		} else {
			that.setData({'now_category.end_time': value});
		}
	},

	onSelectGoodsImage: function() {
		var that = this;
		app.util.image({
			count: 1,
			success: function(file) {
				that.setData({
					'now_category.thumb_': file.url,
					'now_category.thumb': file.filename
				})
			}
		});
	},

	onSubmit: function(e) {
		var that = this;
		var formData = e.detail.value;
		if(!formData.title) {
			app.util.toast('请输入分类名称', '', 1000);
			return false;
		}
		if(formData.is_showtime == 1) {
			if(!formData.start_time) {
				app.util.toast('请选择限购开始时间', '', 1000);
				return false;
			}
			if(!formData.end_time) {
				app.util.toast('请选择限购结束时间', '', 1000);
				return false;
			}
		}
		formData.parentid = that.data.now_category.parentid;
		formData.limit_week = that.data.now_category.limit_week;
		formData.thumb = that.data.now_category.thumb;
		app.util.request({
			url: 'manage/goods/category/post',
			data: {
				id: that.data.options.id || 0,
				params: JSON.stringify(formData),
				formid: e.detail.formId
			},
			method: 'POST',
			success: function (res) {
				var result = res.data.message;
				app.util.toast(result.message);
				if(result.errno) {
					return false;
				}
				wx.navigateTo({
					url: './index'
				});
			}
		});
	},

	onJsEvent: function (e) {
		app.util.jsEvent(e);
	},

	onPullDownRefresh: function () {

	},

	onReachBottom: function () {

	},

})