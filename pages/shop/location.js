var app = getApp();
Page({
	data: {
		showSearch: false,
		MapType: 'gaode',
		Lang: app.Lang,
		wuiLoading: {
			show: true,
			img: app.util.getStorageSync('theme.loading.img')
		}
	},

	onLoad: function (options) {
		var that = this;
		app.util.getLocation(function(result) {
			var location = result.data.message.message;
			that.setData({
				location: location.address,
				location_x: location.location_x,
				location_y: location.location_y,
				pois: location.pois.slice(0,10),
				MapType: app.util.getStorageSync('MapType')
			}, function() {
				app.util.loaded();
			});
		});
	},

	onInput: function(e) {
		var that = this;
		var key = e.detail.value;
		if(!key) {
			that.setData({showSearch: false})
			return;
		}
		var MapType = that.data.MapType;
		var url = 'system/common/map/suggestion';
		if(MapType == 'google') {
			url = 'system/common/map/suggestion_google';
		}
		app.util.request({
			url: url,
			data: {key: key},
			success: function(e) {
				var errno = e.data.message.errno;
				if(errno) {
					return false;
				}
				var searchAddress = e.data.message.message;
				if(searchAddress && searchAddress.length > 0){
					that.setData({
						searchAddress: searchAddress,
						showSearch: true
					})
				}
			}
		});
	},

	onChooseAddress: function(e) {
		var that = this;
		var result = e.currentTarget.dataset;
		if(!result.x || !result.y) {
			app.util.toast('该地址无效');
			return false;
		}
		var location = {
			address: result.address,
			location_x: result.x,
			location_y: result.y
		}
		app.util.setStorageSync('searchLocationInfo', location);
		wx.navigateBack();
	},

	onPullDownRefresh: function() {
		var that = this;
		that.onLoad();
		wx.stopPullDownRefresh();
	}
})