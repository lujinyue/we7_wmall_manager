var app = getApp();
Page({
	data: {
		Lang: app.Lang,

	},
	onLoad: function () {
		var that = this;
		app.util.request({
			url: 'manage/shop/index/info',
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				that.setData(result.message)
			}
		});
	},

	onShowTip: function() {
		wx.showModal({
			content: '您无法修改此项，如需更换门店分类，请联系平台管理员',
			showCancel: false,
		});
	},

	onUploadLogo: function() {
		var that = this;
		app.util.image({
			count: 1,
			success: function(res) {
				var logo = res.filename;
				app.util.request({
					url: 'manage/shop/index/logo',
					methods: 'POST',
					data: {
						logo: logo
					},
					success: function (res) {
						var result = res.data.message;
						if(result.errno) {
							app.util.toast(result.message);
							return false;
						}
						app.util.toast(result.message, 'refresh');
					}
				});
			}
		})
	},

	onJsEvent: function(e) {
		app.util.jsEvent(e);
	},
	onPullDownRefresh: function () {
		this.onLoad();
		wx.stopPullDownRefresh();
	}
})