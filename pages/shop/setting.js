var app = getApp();

Page({
	data: {
		Lang: app.Lang,

	},

	onLoad: function () {
	},

	onChangeSwitch: function(e) {
		var value = e.detail.value ? 1 : 0;
		var type = e.currentTarget.dataset.type;
		app.util.request({
			url: 'manage/shop/index/status',
			method: 'POST',
			data: {
				type: type,
				value: value
			},
			success: function (res) {
				var result = res.data.message;
				app.util.toast(result.message);
				if(result.errno) {
					return false;
				}
			}
		});
	},

	onLoginout: function() {
		wx.showModal({
			title: '',
			content: '确定退出当前登录吗？',
			success: function(res) {
				if(res.confirm) {
					app.util.request({
						url: 'manage/auth/loginout',
						method: 'POST',
						success: function (res) {
							var result = res.data.message;
							if(result.errno) {
								app.util.toast(result.message);
								return false;
							}
							wx.removeStorageSync('clerkInfo');
							wx.removeStorageSync('__sid');
							app.util.toast(result.message, '/pages/auth/index', 1000);
						}
					});
				} else if(res.cancel) {
				}
			}
		});
	},

	onJsEvent: function (e) {
		app.util.jsEvent(e);
	},

	onShow: function () {
		var that = this;
		app.util.request({
			url: 'manage/shop/index',
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				result.message['wxappversion'] = app.util.wxappversion();
				that.setData(result.message);
			}
		});
	},

	onPullDownRefresh: function () {
		this.onShow();
		wx.stopPullDownRefresh();
	}
})