var app = getApp();
Page({
	data: {
		Lang: app.Lang,

	},

	onLoad: function () {
		var that = this;
		app.util.request({
			url: 'manage/shop/index/index',
			success: function (res) {
				var global = res.data.global;
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				result.message['noticeChannel'] = global.noticeChannel;
				that.setData(result.message);
				wx.setNavigationBarTitle({
					title: result.message.store.title
				});
			}
		});
	},

	onAddNoticeNum: function() {
		var that = this;
		app.util.requestSubscribeMessage({
			success: function() {
				app.util.request({
					url: 'manage/shop/index/notice_num',
					method: 'POST',
					success: function(res) {
						var result = res.data.message;
						if(result.errno) {
							app.util.toast(result.message);
							return false;
						}
						that.setData({
							'manager.wxapp_notice_num': result.message.wxapp_notice_num
						})
					}
				});
			}
		});
	},

	onJsEvent: function(e) {
		app.util.jsEvent(e);
	},

	onPullDownRefresh: function () {
		this.onLoad();
		wx.stopPullDownRefresh();
	}
})