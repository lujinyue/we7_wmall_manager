var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		store: {},
		files: [],
		thumbs: [],
		tempThumbs: []
	},

	onLoad: function(options) {
		if(!options.type) {
			return false;
		}
		this.setData({
			options: options,
			type: options.type
		});
	},

	onShow: function () {
		var that = this;
		var type = that.data.type;
		if(!type) {
			return false;
		}
		app.util.request({
			url: 'manage/shop/index/info',
			data: {
				type: that.data.type
			},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				if(type == 'thumbs') {
					if(result.message.store.thumbs.length > 0) {
						that.setData({
							thumbs: result.message.store.thumbs
						});
					}
					return;
				} else if(type == 'address') {
					var searchLocationInfo = app.util.getStorageSync('searchLocationInfo');
					if(searchLocationInfo.address && searchLocationInfo.location_x && searchLocationInfo.location_y) {
						result.message.store.address = searchLocationInfo.address;
						result.message.store.location_x = searchLocationInfo.location_x;
						result.message.store.location_y = searchLocationInfo.location_y;
					}
				}
				that.setData(result.message)
			}
		});
	},

	onGetTextarea: function(e) {
		this.setData({
			textarea: e.detail.value
		})
	},

	onTimeChange: function(e) {
		console.log(e)
		var that = this;
		var index = e.currentTarget.dataset.index;
		var type = e.currentTarget.dataset.type;
		var value = e.detail.value;
		that.data.store.business_hours[index][type] = value;
		that.setData({
			store: that.data.store
		})
	},

	onRemoveTime: function(e) {
		var that = this;
		var index = e.currentTarget.dataset.index;
		that.data.store.business_hours.splice(index, 1);
		that.setData({
			store: that.data.store
		})
	},

	onAddTime: function() {
		var that = this;
		if(that.data.store.business_hours.length >= 3) {
			app.util.toast('最多可添加3个时间段');
			return false;
		}
		var hour = {
			s: '00:00',
			e: '23:59'
		}
		that.data.store.business_hours.push(hour);
		that.setData({
			store: that.data.store
		})
	},

	onSubmit: function(e) {
		var that = this;
		console.log('form发生了submit事件，携带数据为：', e.detail.value)
		var value = '';
		var type = that.data.type;
		if(type == 'title' || type == 'telephone') {
			value = e.detail.value.input;
			if(!value) {
				app.util.toast('信息不能为空');
				return false;
			}
		} else if(type == 'address') {
			value = {
				address: e.detail.value.input,
				location_x: that.data.store.location_x,
				location_y: that.data.store.location_y,
			}
		} else if(type == 'notice' || type == 'content') {
			value = that.data.textarea;
		} else if(type == 'business_hours') {
			value = that.data.store.business_hours
		} else if(type == 'thumbs') {
			var files = that.data.thumbs;
			if(files.length > 0) {
				for(var i = 0; i < files.length; i++) {
					var thumb = {
						image: files[i].filename || files[i].image,
						url: ''
					}
					that.data.tempThumbs.push(thumb);
				}
			}
			value = that.data.tempThumbs;
		} else if(type == 'logo') {

		}
		app.util.request({
			url: 'manage/shop/index/setting',
			method: 'POST',
			data: {
				type: that.data.type,
				value: JSON.stringify(value),
				formid: e.detail.formId
			},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				} else {
					if(that.data.type == 'address') {
						app.util.removeStorageSync('searchLocationInfo');
					}
					app.util.toast('设置成功', '../info', 1000);
				}
			}
		});
	},



	onPullDownRefresh: function () {
		var that = this;
		this.onLoad({type: that.data.type});
		wx.stopPullDownRefresh();
	},

	onReachBottom: function () {

	},

	onJsEvent: function(e) {
		app.util.jsEvent(e);
	},

	onHide: function() {
		app.util.removeStorageSync('searchLocationInfo');
	},
	onUnload: function() {
		app.util.removeStorageSync('searchLocationInfo');
	}
})