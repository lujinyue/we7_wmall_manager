var app = getApp();
Page({
	data: {
		Lang: app.Lang,

	},

	onLoad: function (options) {
		var that = this;
		var from = options.from;
		app.util.request({
			url: 'manage/home/index',
			data: {
				nosid: 1
			},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				var stores = result.message.stores;
				if(!from && stores.length == 1) {
					app.util.setStorageSync('__sid', stores[0].id);
					app.util.jump2url('/pages/order/index');
				} else if(stores.length > 1) {
					that.setData(result.message)
				}
			}
		});
	},

	onSwitch: function(e) {
		var that = this;
		var sid = e.currentTarget.dataset.sid;
		app.util.setStorageSync('__sid', sid);
		app.util.setStorageSync('order_refresh', true);
		app.util.jump2url('/pages/order/index');
		return;
	},

	onPullDownRefresh: function () {

	},

	onReachBottom: function () {

	},

	onJsEvent: function(e) {
		app.util.jsEvent(e);
	}
})