// pages/order/detail.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		codeModalHide: true,
		yinsihao: {
			status: false,
			secret_mobile: '',
			extension: ''
		}
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		var that = this;
		that.data.options = options;
		var id = options.id;
		app.util.request({
			url: 'manage/order/errander/detail',
			data: {id: id},
			showLoading: false,
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message, '', 1000);
					return false;
				}
				that.setData(result.message);
			}
		});

	},

	onCallCustomer: function(e) {
		var that = this;
		var memberType = e.currentTarget.dataset.memberType ? e.currentTarget.dataset.memberType : 'accept';
		app.util.request({
			url: 'yinsihao/yinsihao',
			data: {
				order_id: that.data.order.id,
				type: 'errander',
				ordersn: that.data.order.order_sn,
				orderType: 'errander',
				memberType: memberType
			},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message, '', 1000);
					return false;
				}
				result = result.message;
				that.data.yinsihao.secret_mobile = result.data.secret_mobile;
				that.data.yinsihao.extension = result.data.extension;
				that.data.yinsihao.status = true;
				that.setData({
					yinsihao: that.data.yinsihao
				})
			}
		});
	},

	onToggleYinsihaoStatus: function() {
		var that = this;
		that.setData({
			'yinsihao.status': !that.data.yinsihao.status
		})
	},

	onCallSecretMobile: function() {
		var that =  this;
		that.onToggleYinsihaoStatus();
		wx.makePhoneCall({
			phoneNumber: that.data.yinsihao.secret_mobile
		})
	},

	onChangeOrderStatus: function(e) {
		var that = this;
		var dataset = e.currentTarget.dataset;
		var type = dataset.type;
		if(type == 'delivery_transfer' || type == 'direct_transfer' || type == 'cancel') {
			wx.navigateTo({
				url: './reason?type=' + type + '&id=' + dataset.id + '&status=' + dataset.status
			});
			return false;
		}
		wx.showModal({
			title: '',
			content: dataset.confirm,
			success: function(res) {
				if(res.confirm) {
					if(dataset.type == 'delivery_success' && that.data.verification_code) {
						that.setData({
							codeModalHide: false,
							orderId: dataset.id
						});
						return false;
					}
					app.util.request({
						url: 'manage/order/errander/status',
						data: dataset,
						success: function (res) {
							var result = res.data.message;
							app.util.toast(result.message, '', 1000);
							if(!result.errno) {
								that.onPullDownRefresh();
							}
						}
					});
				} else if(res.cancel) {
				}
			}
		});
	},

	onJsEvent: function (e) {
		app.util.jsEvent(e);
	},

	onCodeConfirm: function() {
		var that = this;
		that.setData({codeModalHide: true});
		if(!that.data.code) {
			app.util.toast('请输入收货码', '', 1000);
			return false;
		}
		if(that.data.code.length != 4) {
			app.util.toast('输入收货码有误', '', 1000);
			return false;
		}
		app.util.request({
			url: 'manage/order/errander/status',
			data: {
				type: 'delivery_success',
				id: that.data.orderId,
				code: that.data.code
			},
			success: function (res) {
				var result = res.data.message;
				app.util.toast(result.message, '', 1000);
				if(!result.errno) {
					that.onPullDownRefresh();
				}
			}
		});
	},

	onCodecancel:function() {
		var that = this;
		that.setData({codeModalHide: true})
	},

	onInput: function(e) {
		var that = this;
		that.data.code = e.detail.value;
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {
		var that = this;
		var id = that.data.options.id;
		var params = {id: id};
		that.onLoad(params);
		wx.stopPullDownRefresh();
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})