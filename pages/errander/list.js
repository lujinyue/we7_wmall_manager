var app = getApp();

Page({
	data: {
		Lang: app.Lang,
		codeModalHide: true,
		status: 1,
		psize: 15,
		showloading: false,
		order: {
			status: 1,
			page: 1,
			list:[],
			empty: 0,
			loaded: 0
		},
		yinsihao: {
			status: false,
			secret_mobile: '',
			extension: ''
		}
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		var that = this;
		that.onReachBottom();
	},

	onJsEvent: function (e) {
		app.util.jsEvent(e);
	},

	onCallCustomer: function(e) {
		var order_id = e.currentTarget.dataset.id;
		var order_sn = e.currentTarget.dataset.ordersn;
		var that = this;
		app.util.request({
			url: 'yinsihao/yinsihao',
			data: {
				order_id: order_id,
				type: 'errander',
				ordersn: order_sn,
				orderType: 'errander'
			},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message, '', 1000);
					return false;
				}
				result = result.message;
				that.data.yinsihao.secret_mobile = result.data.secret_mobile;
				that.data.yinsihao.extension = result.data.extension;
				that.data.yinsihao.status = true;
				that.setData({
					yinsihao: that.data.yinsihao
				})
			}
		});
	},

	onToggleYinsihaoStatus: function() {
		var that = this;
		that.setData({
			'yinsihao.status': !that.data.yinsihao.status
		})
	},

	onCallSecretMobile: function() {
		var that =  this;
		that.onToggleYinsihaoStatus();
		wx.makePhoneCall({
			phoneNumber: that.data.yinsihao.secret_mobile
		})
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function (options) {
		var that = this;
		that.setData({showloading: true});
		app.util.request({
			url: 'manage/order/errander',
			data: {
				psize: that.data.psize,
				page: that.data.order.page,
				status: that.data.status
			},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message, '', 1000);
				}
				var orders = that.data.order.list.concat(result.message.orders);
				that.data.order.list = orders;
				if (!orders.length) {
					that.data.order.empty = 1;
				}
				if(result.message.orders.length < that.data.psize) {
					that.data.order.loaded = 1;
				}
				that.data.order.page++;
				that.setData({
					showloading: false,
					can_collect_order: result.message.can_collect_order,
					activityItem: that.data.order,
					deliveryer: result.message.deliveryer,
					order: that.data.order,
					verification_code: result.message.verification_code
				});
			}
		});
	},

	onChangeOrderStatus: function(e) {
		var that = this;
		var dataset = e.currentTarget.dataset;
		wx.showModal({
			title: '',
			content: dataset.confirm,
			success: function(res) {
				if(res.confirm) {
					if(dataset.type == 'delivery_success' && that.data.verification_code) {
						that.setData({
							codeModalHide: false,
							orderId: dataset.id
						});
						return false;
					}
					app.util.request({
						url: 'manage/order/errander/status',
						data: dataset,
						success: function (res) {
							var result = res.data.message;
							app.util.toast(result.message, '', 1000);
							if(!result.errno) {
								that.onPullDownRefresh();
							}
						}
					});
				} else if(res.cancel) {
				}
			}
		});
	},

	onChange: function(e) {
		var that = this;
		var status = e.currentTarget.dataset.index;
		if(status == that.data.status) {
			return false;
		}
		that.data.order = {
			status: status,
			page: 1,
			list:[],
			empty: 0,
			loaded: 0
		}
		that.setData({
			status: status
		});
		that.onReachBottom();
	},

	onDetail: function(e) {
		var that = this;
		var dataset = e.currentTarget.dataset;
		var order_id = dataset.id;
		var order_status = dataset.status;
		if(order_status == 1) {
			app.util.toast('抢单后才能查看订单详情', '', 1000);
			return false;
		} else {
			wx.navigateTo({
				url: './detail?id=' + order_id
			})
		}
	},

	onCodeConfirm: function() {
		var that = this;
		that.setData({codeModalHide: true});
		if(!that.data.code) {
			app.util.toast('请输入收货码', '', 1000);
			return false;
		}
		if(that.data.code.length != 4) {
			app.util.toast('输入收货码有误', '', 1000);
			return false;
		}
		app.util.request({
			url: 'manage/order/errander/status',
			data: {
				type: 'delivery_success',
				id: that.data.orderId,
				code: that.data.code
			},
			success: function (res) {
				var result = res.data.message;
				app.util.toast(result.message, '', 1000);
				if(!result.errno) {
					that.onPullDownRefresh();
				}
			}
		});
	},

	onCodecancel:function() {
		var that = this;
		that.setData({codeModalHide: true})
	},

	onInput: function(e) {
		var that = this;
		that.data.code = e.detail.value;
	},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {
		var that = this;
		that.data.order = {
			status: that.data.status,
			page: 1,
			list:[],
			empty: 0,
			loaded: 0
		};
		that.onReachBottom();
		wx.stopPullDownRefresh();
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})