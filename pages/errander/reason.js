// pages/order/reason.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		checked: 0
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		var that = this;
		that.data.options = options;
		app.util.request({
			url: 'delivery/order/errander/op',
			data: {
				type: options.type,
				id: options.id
			},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message, '', 1000);
					return false;
				}
				that.setData({
					reasons: result.message,
					type: that.data.options.type
				});
				if(that.data.type == 'delivery_transfer') {
					var title = '转单理由';
				} else if(that.data.type == 'cancel') {
					title = '取消订单理由';
				} else if(that.data.type == 'direct_transfer') {
					title = '选择配送员';
				}
				wx.setNavigationBarTitle({
					title: title
				});
			}
		});
	},

	onChoose: function(e) {
		var checked = e.detail.value;
		this.setData({
			checked: checked
		})

	},
	onNote: function(e){
		var that = this;
		that.setData({
			note: e.detail.value
		});
	},

	onSubmit: function() {
		var that = this;
		var checked = that.data.checked;
		var type = that.data.type;
		if(type == 'direct_transfer') {
			var deliveryer_id = that.data.reasons[checked].id;
			if(!deliveryer_id) {
				app.util.toast('请选择要转单到的配送员', '', 1000);
				return false;
			}
		} else {
			if(checked == -1) {
				var reason = that.data.note;
			} else{
				if(!that.data.reasons) {
					app.util.toast('请输入原因', '', 1000);
					return false;
				}
				var reason = that.data.reasons[checked];
			}
			if(!reason) {
				app.util.toast('请先输入原因', '', 1000);
				return false;
			}
		}
		var params = {
			id: that.data.options.id,
			type: type,
			deliveryer_id: deliveryer_id,
			reason: reason
		};
		app.util.request({
			url: 'delivery/order/errander/status',
			data: params,
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message, '', 1000);
					return false;
				}
				app.util.toast(result.message, './list', 1000);
			}
		});
	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	}
})