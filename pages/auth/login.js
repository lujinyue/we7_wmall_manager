var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		
	},
	onLoad: function (options) {
		wx.removeStorageSync('timer');
		var clerkInfo = app.util.getStorageSync('clerkInfo');
		if(clerkInfo && clerkInfo.token) {
			app.util.jump2url('/pages/order/index');
			return;
		}
		var that = this;
		app.util.request({
			url: 'manage/auth/login',
			data: {
				nosid: 1
			},
			success: function(res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				that.setData(result.message)
			}
		});
	},
	onSubmit: function(e) {
		var values = e.detail.value;
		if(!values.mobile) {
			app.util.toast('请输入手机号', '', 1000);
			return;
		}
		if(!values.password) {
			app.util.toast('请输入密码', '', 1000);
			return;
		}
		app.util.request({
			url: 'manage/auth/login',
			method: 'POST',
			data: {
				mobile: values.mobile,
				password: values.password,
				nosid: 1,
				formid: e.detail.formId
			},
			success: function(res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				app.util.setStorageSync('clerkInfo', {token: result.message.clerk.token});
				if(result.message.sids.length == 1) {
					app.util.setStorageSync('__sid', result.message.sids[0]);
					app.util.toast('登录成功', '/pages/order/index', 1000);
				} else if(result.message.sids.length > 1) {
					app.util.toast('登录成功', '/pages/shop/select', 1000);
				}
			}
		});
	},
	onJsEvent: function(e) {
		app.util.jsEvent(e);
	}
})