var app = getApp();

Page({
	data: {

	},

	onLoad: function () {
		var clerkInfo = app.util.getStorageSync('clerkInfo');
		if(clerkInfo && clerkInfo.token) {
			app.util.jump2url('/pages/order/index');
			return;
		}
		var that = this;
		app.util.request({
			url: 'system/common/auth/login_bg',
			data: {
				type: 'manager',
				nosid: 1
			},
			success: function(res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				that.setData(result.message)
			}
		});
	},

	onJsEvent: function(e) {
		app.util.jsEvent(e);
	}
})