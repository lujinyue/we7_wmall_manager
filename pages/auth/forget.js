var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		getCode: true,
		code: {text : '获取验证码', downcount: 60}
	},

	onLoad: function (options) {
		this.onRefreshCaptcha();
	},

	onMobile: function(e) {
		this.setData({mobile: e.detail.value});
	},

	onCaptcha: function(e) {
		this.setData({forget_captcha: e.detail.value});
	},

	getCode: function () {
		var that = this;
		var code = that.data.code;
		if(!that.data.getCode) {
			return false;
		}
		if(!that.data.mobile) {
			app.util.toast('手机号不能为空');
			return false;
		}
		if(!app.util.isMobile(that.data.mobile)) {
			app.util.toast('手机号格式错误');
			return false;
		}
		if(!that.data.forget_captcha) {
			app.util.toast('请输入图形验证码');
			return false;
		}
		var params = {
			mobile: that.data.mobile,
			captcha: that.data.forget_captcha,
			nosid: 1
		}
		app.util.request({
			url: 'system/common/code',
			data: params,
			method: 'POST',
			success: function(res){
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					if(result.errno == -10) {
						that.onRefreshCaptcha();
					}
					return false;
				} else {
					code.text = code.downcount + "秒后重新获取";
					that.setData({code: code, getCode: false});
					var timer = setInterval(function(){
						code.downcount--;
						if(code.downcount <= 0){
							clearInterval(timer);
							code.text = "获取验证码";
							code.downcount = 60;
							that.setData({getCode: true});
						} else {
							code.text = code.downcount + "秒后重新获取";
						}
						that.setData({code: code});
					}, 1000);
					app.util.toast('验证码发送成功, 请注意查收');
				}
			}
		})
	},
	onSubmit: function(e) {
		var that = this;
		var values = e.detail.value;
		if(!values.mobile){
			app.util.toast('手机号不能为空');
			return false;
		}
		if(!app.util.isMobile(values.mobile)) {
			app.util.toast('手机号格式错误');
			return false;
		}
		if(!values.code) {
			app.util.toast('请输入短信验证码');
			return false;
		}
		if (!values.password) {
			app.util.toast('密码不能为空');
			return false;
		} else {
			var length = values.password.length;
			if(length < 8 || length > 20) {
				app.util.toast("请输入8-20位密码");
				return false;
			}
			var reg = /[0-9]+[a-zA-Z]+[0-9a-zA-Z]*|[a-zA-Z]+[0-9]+[0-9a-zA-Z]*/;
			if(!reg.test(values.password)) {
				app.util.toast("密码必须由数字和字母组合");
				return false;
			}
		}
		if (!values.repassword) {
			app.util.toast('请重复输入密码');
			return false;
		}
		if(values.password != values.repassword) {
			app.util.toast('两次密码输入不一致');
			return false;
		}
		var params = {
			mobile: values.mobile,
			code: values.code,
			password: values.password,
			repassword: values.repassword,
			formid: e.detail.formId,
			nosid: 1,
		}
		app.util.request({
			url: 'manage/auth/forget',
			data: params,
			method: 'POST',
			success: function (result) {
				if (result.data.message.errno == 0) {
					app.util.toast(result.data.message.message, 'login', 1000);
				} else {
					app.util.toast(result.data.message.message);
				}
			}
		});
	},
	onRefreshCaptcha: function() {
		var that = this;
		app.util.request({
			url: 'manage/auth/forget/captcha',
			data: {
				nosid: 1
			},
			success: function (res) {
				that.setData({captcha: res.data.message.message.captcha});
			}
		})
	}
})