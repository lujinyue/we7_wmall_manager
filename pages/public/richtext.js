// pages/public/richtext.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,

	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		var that = this;
		var key = options.key;
		var params = {
			key: key
		};
		if(options.pageid) {
			params.pageid = options.pageid;
		}
		if(options.helpid) {
			params.helpid = options.helpid;
		}
		if(options.key = 'notice') {
			params.noticeid = options.noticeid;
		}
		app.util.request({
			url: 'wmall/common/agreement',
			data: params,
			success: function(res) {
				var agreement = res.data.message.message.agreement;
				app.WxParse.wxParse('agreement', 'html', agreement, that, 5);
				wx.setNavigationBarTitle({
					title: res.data.message.message.title
				})
			}
		});

	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})