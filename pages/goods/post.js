var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		goodsTemp: {},
		columns: [],
	},

	onLoad: function (options) {
		var that = this;
		that.data.options = options;
		app.util.request({
			url: 'manage/goods/index/post',
			data: {id: options.id || 0},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				that.data.goodsTemp = result.message.goods;
				if(!that.data.goodsTemp.unitnum) {
					that.data.goodsTemp.unitnum = 1;
				}

				var plateformCategoryStatus = result.message.plateform_goods_category_status;
				if(plateformCategoryStatus) {
					var plateformCategorys = result.message.plateform_categorys;
					that.data.columns[0] = plateformCategorys;
					if(plateformCategorys[0].child) {
						that.data.columns[1] = plateformCategorys[0]['child'];
					}
					var plateform_category_index = result.message.plateform_category_index;
					var plateform_category_child_index = result.message.plateform_category_child_index;
					if(plateform_category_index > 0) {
						that.data.columns[1] = plateformCategorys[plateform_category_index]['child'];
					}
					var multiIndex = [plateform_category_index, plateform_category_child_index];
					if(that.data.goodsTemp.plateform_cid > 0) {
						that.data.goodsTemp['plateformCategoryTitle'] = plateformCategorys[plateform_category_index]['title'];
						if(that.data.goodsTemp.plateform_child_id > 0) {
							that.data.goodsTemp['plateformCategoryTitle'] += '-' + plateformCategorys[plateform_category_index]['child'][plateform_category_child_index]['title'];
						}
					}
				}

				var packtypeStatus = result.message.packtype_status;
				var packtypes = [];
				var packtypeIndex = 0;
				if(packtypeStatus) {
					packtypes = result.message.packtypes;
					packtypeIndex = result.message.packtype_index;
					if(that.data.goodsTemp.packtype_id > 0) {
						that.data.goodsTemp.packtypeTitle = packtypes[packtypeIndex].title
					}
				}

				that.setData({
					goodsTemp: that.data.goodsTemp,
					plateformCategoryStatus: plateformCategoryStatus,
					plateformCategorys: plateformCategorys,
					categorys: result.message.categorys,
					packtypeStatus: packtypeStatus,
					packtypes: packtypes,
					packtypeIndex: packtypeIndex,
					type: result.message.type,
					huangou_types: result.message.huangou_types,
					columns: that.data.columns,
					multiIndex: multiIndex
				});
			}
		});
	},

	onSelectPlateformCategory: function(e) {
		var that = this;
		var parentIndex = e.detail.value[0];
		var childIndex = e.detail.value[1];
		var columns = that.data.columns;
		that.data.goodsTemp.plateformCategoryTitle = columns[0][parentIndex].title;
		that.data.goodsTemp.plateform_cid = columns[0][parentIndex].id;
		if(columns[1] && columns[1][childIndex]) {
			that.data.goodsTemp.plateformCategoryTitle += '-' + columns[1][childIndex].title
			that.data.goodsTemp.plateform_child_id = columns[1][childIndex].id;
		}
		that.setData({
			'goodsTemp.plateformCategoryTitle': that.data.goodsTemp.plateformCategoryTitle,
			'goodsTemp.plateform_cid': that.data.goodsTemp.plateform_cid,
			'goodsTemp.plateform_child_id': that.data.goodsTemp.plateform_child_id,
		});
	},

	onChangeColumn: function(e) {
		var that = this;
		var columnIndex = e.detail.column;
		if(columnIndex == 0) {
			var itemIndex = e.detail.value;
			var columns = that.data.columns;
			if(columns[0][itemIndex].child) {
				columns[1] = columns[0][itemIndex].child;
			} else {
				columns[1] = [];
			}
			that.setData({columns: columns});
		}
		return;
	},

	onSelectPacktype: function(event) {
		var that = this;
		var index = event.detail.value;
		var packtype = that.data.packtypes[index];
		that.setData({
			'goodsTemp.packtype_id': packtype.id,
			'goodsTemp.packtypeTitle': packtype.title
		});
	},

	onSelectCategory: function(event) {
		var that = this;
		var index = event.detail.value;
		var selectCategory = that.data.categorys[index];
		that.setData({
			'goodsTemp.cid': selectCategory.parentid > 0 ? selectCategory.parentid : selectCategory.id,
			'goodsTemp.child_id': selectCategory.parentid > 0 ? selectCategory.id : 0,
			'goodsTemp.category_title': selectCategory.title
		});
	},

	onSelectType: function(event) {
		var that = this;
		var index = event.detail.value;
		var selectType = that.data.type[index];
		that.setData({
			'goodsTemp.type': selectType.id,
			'goodsTemp.type_title': selectType.title
		});
	},

	onSelectHuangou: function(event) {
		var that = this;
		var index = event.detail.value;
		var huangouType = that.data.huangou_types[index];
		that.setData({
			'goodsTemp.huangou_type': huangouType.id,
			'goodsTemp.huangou_title': huangouType.title
		});
	},

	switch1Change: function(event) {
		var that = this;
		var name = event.currentTarget.dataset.name;
		var value = event.detail.value;
		value = value ? 1 : 0;
		if(name == 'status') {
			that.setData({
				'goodsTemp.status': value
			});
		} else if(name == 'total_auto_update') {
			that.setData({
				'goodsTemp.total_auto_update': value
			});
		} else {
			that.setData({
				'goodsTemp.is_hot': value
			});
		}
	},

	onSelectGoodsImage: function() {
		var that = this;
		app.util.image({
			count: 1,
			success: function(file) {
				that.setData({
					'goodsTemp.thumb_': file.url,
					'goodsTemp.thumb': file.filename
				})
			}
		});
	},

	onChangeOptionOrAtrr: function(e) {
		var that = this;
		var type = e.currentTarget.dataset.type;
		var index = e.currentTarget.dataset.index;
		var key = e.currentTarget.dataset.key;
		var value = e.detail.value;
		that.data.goodsTemp[type][index][key] = value;
		if(type == 'options') {
			that.setData({
				'goodsTemp.options': that.data.goodsTemp.options
			})
		} else {
			that.setData({
				'goodsTemp.attrs': that.data.goodsTemp.attrs
			})
		}
	},

	onAddOptionsOrAttrs: function(e) {
		var that = this;
		var type = e.currentTarget.dataset.type;
		if(type == 'options') {
			var optionItem = {
				id: 0,
				name: '',
				price: 0,
				svip_price: 0,
				total: -1,
				total_warning: 0,
				total_everyday: 0,
				total_auto_update: 0,
				displayorder: 0,
			};
			if(!that.data.goodsTemp.options) {
				that.data.goodsTemp.options = [];
			}
			that.data.goodsTemp.options.push(optionItem);
			that.setData({
				'goodsTemp.options': that.data.goodsTemp.options
			})
		} else {
			var	attrsItem = {
				name: '',
				goodsLabel: '',
				label: []
			};
			if(!that.data.goodsTemp.attrs) {
				that.data.goodsTemp.attrs = [];
			}
			that.data.goodsTemp.attrs.push(attrsItem);
			that.setData({
				'goodsTemp.attrs': that.data.goodsTemp.attrs
			})
		}
	},

	onDeleteOptionsOrAttrs: function(e) {
		var that = this;
		var type = e.currentTarget.dataset.type;
		var index = e.currentTarget.dataset.index;
		that.data.goodsTemp[type].splice(index, 1);
		if(type == 'options') {
			that.setData({
				'goodsTemp.options': that.data.goodsTemp.options
			})
		} else {
			that.setData({
				'goodsTemp.attrs': that.data.goodsTemp.attrs
			})
		}
	},

	onAddAttrsLabel: function(e) {
		var that = this;
		var index = e.currentTarget.dataset.index;
		if(!that.data.goodsTemp.attrs[index].goodsLabel) {
			app.util.toast('请输入有效的属性标签');
			return false;
		}
		that.data.goodsTemp.attrs[index].label.push(that.data.goodsTemp.attrs[index].goodsLabel);
		that.data.goodsTemp.attrs[index].goodsLabel = '';
		that.setData({
			'goodsTemp.attrs': that.data.goodsTemp.attrs
		})
	},

	onDeleteAttrsLabel: function(e) {
		var that = this;
		var index = e.currentTarget.dataset.index;
		var labIndex = e.currentTarget.dataset.labIndex;
		that.data.goodsTemp.attrs[index].label.splice(labIndex, 1);
		that.setData({
			'goodsTemp.attrs': that.data.goodsTemp.attrs
		})
	},

	onSubmit: function(e) {
		var that = this;
		var formData = e.detail.value;
		if(!formData.title) {
			app.util.toast('商品名称不能为空', '', 1000);
			return false;
		}
		if(!formData.price) {
			app.util.toast('商品外卖价格不能为空', '', 1000);
			return false;
		}
		if(!formData.ts_price) {
			app.util.toast('商品店内价格不能为空', '', 1000);
			return false;
		}
		if(that.data.plateformCategoryStatus) {
			formData.plateform_cid = parseInt(that.data.goodsTemp.plateform_cid) + 0;
			if(!formData.plateform_cid) {
				app.util.toast('请选择平台商品分类', '', 1000);
				return false;
			}
			formData.plateform_child_id = that.data.goodsTemp.plateform_child_id;
		}
		formData.cid = that.data.goodsTemp.cid;
		if(!formData.cid) {
			app.util.toast('请选择商品所属分类', '', 1000);
			return false;
		}
		if(that.data.packtypeStatus) {
			formData.packtype_id = that.data.goodsTemp.packtype_id - 0;
			if(!formData.packtype_id) {
				app.util.toast('请选择商品包装方式', '', 1000);
				return false;
			}
		}
		formData.type = that.data.goodsTemp.type;
		if (!formData.type) {
			app.util.toast('请选择商品所属类型', '', 1000);
			return false;
		}

		formData.huangou_type = that.data.goodsTemp.huangou_type;
		formData.thumb = that.data.goodsTemp.thumb;
		formData.child_id = that.data.goodsTemp.child_id;
		formData.options = that.data.goodsTemp.options;
		formData.attrs = that.data.goodsTemp.attrs;
		app.util.request({
			url: 'manage/goods/index/post',
			data: {
				id: that.options.id || 0,
				params: JSON.stringify(formData),
				formid: e.detail.formId
			},
			method: 'POST',
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				app.util.toast(result.message, './index', 1000);
			}
		});
	},

	onJsEvent: function (e) {
		app.util.jsEvent(e);
	}
})