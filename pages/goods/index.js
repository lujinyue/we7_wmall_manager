var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		goodsAll: {},
		goodsItem: {},
		category_goodsNum: {}
	},

	onLoad: function (options) {
		var that = this;
		that.data.options = options;
		var cid = options.cid || 0;
		that.onGetGoods(cid);
	},

	onToggleCategory:function(e) {
		var that = this;
		var cid = e.currentTarget.dataset.id;
		that.data.selectCategory = that.data.categorys[cid];
		that.data.selectCategory.goods_num = that.data.category_goodsNum[cid];
		that.setData({
			selectCategory: that.data.selectCategory,
			selectCategoryId: cid
		});
		that.onGetGoods(cid);
	},

	onGetGoods: function(cid) {
		var that = this;
		if(that.data.goodsAll[cid]) {
			that.setData({goodsItem: that.data.goodsAll[cid]});
			return false;
		}
		app.util.request({
			url: 'manage/goods/index/list',
			data: {
				cid: cid
			},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				that.data.goodsItem = result.message.goods;
				that.data.selectCategoryId = result.message.cid;
				that.data.goodsAll[that.data.selectCategoryId] = that.data.goodsItem;
				that.data.selectCategory = result.message.categorys[that.data.selectCategoryId];
				that.data.selectCategory.goods_num = result.message.goods_num;
				that.data.category_goodsNum[that.data.selectCategoryId] = result.message.goods_num;
				that.setData({
					categorys: result.message.categorys,
					goodsItem: result.message.goods,
					selectCategoryId: result.message.cid,
					selectCategory: that.data.selectCategory
				});
			}

		});
	},

	onJsEvent: function (e) {
		app.util.jsEvent(e);
	},

	onChangeStatus: function(e) {
		var that = this;
		var dataset = e.currentTarget.dataset;
		var id = dataset.id;
		var type = dataset.type;
		var value = dataset.value;
		var cid = dataset.cid;
		var index = dataset.index;
		var url = '';
		var params = {};
		var confirm = '';
		if(type == 'status') {
			url = 'manage/goods/index/status';
			confirm = value == 1 ? '确定下架此商品吗?' : '确定上架此商品吗?';
			params = {
				id: id,
				value: value == 1 ? 0 : 1
			}
		} else if(type == 'total') {
			url = 'manage/goods/index/turncate';
			confirm = '确定设置已售罄吗?';
			params = {id: id}
		}
		wx.showModal({
			title: '',
			content: confirm,
			success: function(res) {
				if(res.confirm) {
					app.util.request({
						url: url,
						data: params,
						showloading: false,
						success: function (res) {
							var result = res.data.message;
							app.util.toast(result.message);
							if(result.errno) {
								return false;
							}
							if(type == 'status') {
								that.data.goodsAll[cid][index].status = value == 1 ? 0 : 1;
								that.setData({goodsItem: that.data.goodsAll[cid]});
							} else if(type == 'total') {
								that.data.goodsAll[cid][index].total = 0;
								that.setData({goodsItem: that.data.goodsAll[cid]});
							}
						}

					});
				} else if(res.cancel) {
				}
			}
		});
	},

	onShowAuditReason: function(e) {
		var that = this;
		var reason = e.currentTarget.dataset.reason;
		wx.showModal({
			title: '审核未通过原因',
			content : reason,
			showCancel : false
		});
	},

	onPullDownRefresh: function () {
		var cid = this.data.selectCategoryId;
		delete(this.data.goodsAll[cid]);
		this.onGetGoods(cid);
		wx.stopPullDownRefresh();
	},

	onReachBottom: function () {

	}
})