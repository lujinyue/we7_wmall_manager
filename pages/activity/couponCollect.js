// pages/activity/couponCollect.js
var app = getApp();
var dateTimePicker = require('../../static/js/utils/dateTimePicker.js');
Page({
	data: {
		Lang: app.Lang,
		startYear: 2018,
		endYear: 2030,
		coupon: {},
		coupons: [],
		type_limit: 1
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		var that = this;
		app.util.request({
			url: 'manage/activity/index/activity_coupon',
			data: {type: options.type || 'couponCollect'},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					if(result.errno == -1000) {
						app.util.toast(result.message, 'redirect:/pages/activity/index', 1000);
						return false;
					}
					app.util.toast(result.message);
					return false;
				}
				var obj = dateTimePicker.dateTimePicker(that.data.startYear, that.data.endYear);
				// 精确到分的处理，将数组的秒去掉,dateArr日期数据， arr各列index
				var dateArr = obj.dateTimeArray;
				var arr = obj.dateTime;
				dateArr.pop();
				arr.pop();
				that.data.starttime = dateArr[0][arr[0]] + '-' + dateArr[1][arr[1]] + '-' + dateArr[2][arr[2]] + ' ' + dateArr[3][arr[3]] + ':' +dateArr[4][arr[4]];
				that.setData({
					starttime: that.data.starttime,
					endtime: that.data.starttime,
					dateTime: arr,
					dateTimeArray: dateArr,
					type: result.message.type,
					page_title: result.message.page_title
				});
				wx.setNavigationBarTitle({
					title: that.data.page_title
				});
			}
		});
	},

	changeDateTime: function(e){
		this.setData({ dateTime: e.detail.value });
	},
	changeDateTimeColumn: function(e){
		var that = this;
		var arr = that.data.dateTime, dateArr = that.data.dateTimeArray;
		arr[e.detail.column] = e.detail.value;
		dateArr[2] = dateTimePicker.getMonthDay(dateArr[0][arr[0]], dateArr[1][arr[1]]);
		var type = e.currentTarget.dataset.type;
		var time_cn = dateArr[0][arr[0]] + '-' + dateArr[1][arr[1]] + '-' + dateArr[2][arr[2]] + ' ' + dateArr[3][arr[3]] + ':' +dateArr[4][arr[4]];
		if(type == 'starttime') {
			that.setData({starttime: time_cn});
		} else if(type == 'endtime') {
			that.setData({endtime: time_cn});
		}
	},

	onSelectGroup: function(e) {
		var that = this;
		var value = e.detail.value;
		that.setData({type_limit: value});
	},

	onAddCoupon: function(e) {
		var that = this;
		if(that.data.type == 'couponGrant') {
			if(that.data.coupons.length >= 1) {
				app.util.toast('最多添加1张代金券', '', 1000);
				return false;
			}
		}
		if(that.data.coupons.length >= 3) {
			app.util.toast('最多添加3张代金券', '', 1000);
			return false;
		}
		if(e.currentTarget.dataset.type == 'cancel') {
			that.setData({addCoupon: false});
			return false;
		}
		that.setData({addCoupon: true});
	},

	onDelCoupon: function(e) {
		var that = this;
		var index = e.currentTarget.dataset.index;
		that.data.coupons.splice(index, 1);
		that.setData({coupons: that.data.coupons});
	},

	onEditCoupon: function(e) {
		var that = this;
		var index = e.currentTarget.dataset.index;
		that.data.coupon = that.data.coupons[index];
		that.setData({
			coupon: that.data.coupon,
			addCoupon: true
		});
	},

	onSubmit: function(e) {
		var that = this;
		var formData = e.detail.value;
		if(!formData.title) {
			app.util.toast('活动名称不能为空', '', 1000);
			return false;
		}
		if(!formData.amount) {
			app.util.toast('券包总数不能为空', '', 1000);
			return false;
		}
		if(that.data.type == 'couponCollect' && !formData.type_limit) {
			app.util.toast('请选择面向人群', '', 1000);
			return false;
		}
		if(!that.data.coupons.length) {
			app.util.toast('请添加优惠券', '', 1000);
			return false;
		}
		formData.coupons = that.data.coupons;
		formData.starttime = that.data.starttime;
		formData.endtime = that.data.endtime;
		app.util.request({
			url: 'manage/activity/index/activity_coupon',
			data: {
				type:that.data.type,
				params: JSON.stringify(formData),
				formid: e.detail.formId
			},
			method: 'POST',
			success: function (res) {
				var result = res.data.message;
				app.util.toast(result.message);
				if(result.errno) {
					return false;
				}
				wx.navigateTo({url:'./index'});
			}
		});
	},

	onCoupon: function(e) {
		var that = this;
		that.data.coupon = e.detail.value;
		if(!that.data.coupon.discount || that.data.coupon.discount < 0) {
			app.util.toast('请填写优惠金额', '', 1000);
			return false;
		} else if(!that.data.coupon.condition || that.data.coupon.condition < 0 || that.data.coupon.condition - that.data.coupon.discount < 0) {
			app.util.toast('请填写使用条件', '', 1000);
			return false;
		} else if(!that.data.coupon.use_days_limit || that.data.coupon.use_days_limit < 0) {
			app.util.toast('请填写有效期', '', 1000);
			return false;
		}
		that.data.coupons.push(that.data.coupon);
		that.setData({
			coupons: that.data.coupons,
			addCoupon: false
		});
	},
	/**
	* 生命周期函数--监听页面初次渲染完成
	*/
	onReady: function () {

	},

	/**
	* 生命周期函数--监听页面显示
	*/
	onShow: function () {

	},

	/**
	* 生命周期函数--监听页面隐藏
	*/
	onHide: function () {

	},

	/**
	* 生命周期函数--监听页面卸载
	*/
	onUnload: function () {

	},

	/**
	* 页面相关事件处理函数--监听用户下拉动作
	*/
	onPullDownRefresh: function () {

	},

	/**
	* 页面上拉触底事件的处理函数
	*/
	onReachBottom: function () {

	},

	/**
	* 用户点击右上角分享
	*/
	onShareAppMessage: function () {

	}
})