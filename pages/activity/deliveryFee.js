// pages/activity/deliveryFee.js
var app = getApp();
var dateTimePicker = require('../../static/js/utils/dateTimePicker.js');
Page({
	data: {
		Lang: app.Lang,
		options: {},
		startYear: 2018,
		endYear: 2030
	},

	/**
	* 生命周期函数--监听页面加载
	*/
	onLoad: function (options) {
		var that = this;
		app.util.request({
			url: 'manage/activity/index/activity_other',
			data: {type: options.type},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					if(result.errno == -1000) {
						app.util.toast(result.message, 'redirect:/pages/activity/index', 1000);
						return false;
					}
					app.util.toast(result.message);
					return false;
				}
				var obj = dateTimePicker.dateTimePicker(that.data.startYear, that.data.endYear);
				// 精确到分的处理，将数组的秒去掉,dateArr日期数据， arr各列index
				var dateArr = obj.dateTimeArray;
				var arr = obj.dateTime;
				dateArr.pop();
				arr.pop();
				that.data.starttime = dateArr[0][arr[0]] + '-' + dateArr[1][arr[1]] + '-' + dateArr[2][arr[2]] + ' ' + dateArr[3][arr[3]] + ':' +dateArr[4][arr[4]];
				that.setData({
					starttime: that.data.starttime,
					endtime: that.data.starttime,
					dateTime: arr,
					dateTimeArray: dateArr,
					type: result.message.type,
					page_title: result.message.page_title,
					discount_title: result.message.discount_title,
					discount_cn: result.message.discount_cn
				});
				wx.setNavigationBarTitle({
					title: that.data.page_title
				});
			}
		});
	},

	changeDateTime: function(e){
		this.setData({ dateTime: e.detail.value });
	},
	changeDateTimeColumn: function(e){
		var that = this;
		var arr = that.data.dateTime, dateArr = that.data.dateTimeArray;
		arr[e.detail.column] = e.detail.value;
		dateArr[2] = dateTimePicker.getMonthDay(dateArr[0][arr[0]], dateArr[1][arr[1]]);
		var type = e.currentTarget.dataset.type;
		var time_cn = dateArr[0][arr[0]] + '-' + dateArr[1][arr[1]] + '-' + dateArr[2][arr[2]] + ' ' + dateArr[3][arr[3]] + ':' +dateArr[4][arr[4]];
		if(type == 'starttime') {
			that.setData({starttime: time_cn});
		} else if(type == 'endtime') {
			that.setData({endtime: time_cn});
		}
	},

	onInput: function(e) {
		var that = this;
		var index = e.currentTarget.dataset.index;
		var type = e.currentTarget.dataset.type;
		var value = e.detail.value;
		if(!that.data.options[index]) {
			that.data.options[index] = {};
		}
		that.data.options[index][type] = value;
	},

	onSubmit: function(e) {
		var that = this;
		if(!that.data.type) {
			app.util.toast('请选择活动类型', '', 1000);
			return false;
		}
		var params = {
			options: that.data.options,
			starttime: that.data.starttime,
			endtime: that.data.endtime
		};
		app.util.request({
			url: 'manage/activity/index/activity_other',
			data: {
				type: that.data.type,
				params: JSON.stringify(params)
			},
			method: 'POST',
			success: function (res) {
				var result = res.data.message;
				app.util.toast(result.message);
				if(result.errno) {
					return false;
				}
				wx.navigateTo({url:'./index'});
			}
		});
	},

	/**
	* 生命周期函数--监听页面初次渲染完成
	*/
	onReady: function () {

	},

	/**
	* 生命周期函数--监听页面显示
	*/
	onShow: function () {

	},

	/**
	* 生命周期函数--监听页面隐藏
	*/
	onHide: function () {

	},

	/**
	* 生命周期函数--监听页面卸载
	*/
	onUnload: function () {

	},

	/**
	* 页面相关事件处理函数--监听用户下拉动作
	*/
	onPullDownRefresh: function () {

	},

	/**
	* 页面上拉触底事件的处理函数
	*/
	onReachBottom: function () {

	},

	/**
	* 用户点击右上角分享
	*/
	onShareAppMessage: function () {

	}
})