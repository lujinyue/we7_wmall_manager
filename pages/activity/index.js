var app = getApp();
Page({
	data: {
		Lang: app.Lang,

	},

	onLoad: function () {
		var that = this;
		app.util.request({
			url: 'manage/activity/index',
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				that.setData(result.message);
			}
		});
	},

	onJsEvent: function (e) {
		app.util.jsEvent(e);
	},
	onPullDownRefresh: function () {
		this.onLoad();
		wx.stopPullDownRefresh();
	}

})