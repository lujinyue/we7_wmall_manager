var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		pay_type: 'all',
		orders: {
			page: 1,
			psize: 15,
			empty: false,
			loaded: false,
			data: []
		}
	},

	onLoad: function(options) {
		var that = this;
		that.onReachBottom();
		return;
	},

	onChangeType: function(e) {
		var that = this;
		var pay_type = e.currentTarget.dataset.type;
		if(pay_type == that.data.pay_type) {
			return false;
		}
		that.setData({
			pay_type: pay_type,
			orders: {
				page: 1,
				psize: 15,
				empty: false,
				loaded: false,
				data: []
			}
		});
		that.onReachBottom();
	},

	onPullDownRefresh: function () {
		var that = this;
		that.setData({
			pay_type: 'all',
			orders: {
				page: 1,
				psize: 15,
				empty: false,
				loaded: false,
				data: []
			}
		});
		that.onLoad();
		wx.stopPullDownRefresh();
	},

	onReachBottom: function () {
		var that = this;
		if(that.data.orders.loaded) {
			return false;
		}
		app.util.request({
			url: 'manage/paybill/index',
			data: {
				pay_type: that.data.pay_type,
				page: that.data.orders.page,
				psize: that.data.orders.psize,
			},
			success: function(res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return;
				}
				result = result.message;
				var orders = that.data.orders.data.concat(result.orders);
				that.data.orders.data = orders;
				that.data.orders.page++;
				if(!orders.length) {
					that.data.orders.empty = true;
				}
				if(result.orders.length < that.data.orders.psize) {
					that.data.orders.loaded = true;
				}
				that.setData({
					orders: that.data.orders,
				})
			}
		})
	},

	onJsEvent: function(e) {
		app.util.jsEvent(e);
	},
})