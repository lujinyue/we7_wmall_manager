// pages/advertise/index.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,

	},

	/**
	* 生命周期函数--监听页面加载
	*/
	onLoad: function (options) {
		var that = this;
		app.util.request({
			url: 'manage/advertise/index',
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				that.setData(result.message);
			}
		});
	},
	onJsEvent: function (e) {
		app.util.jsEvent(e);
	},

	/**
	* 生命周期函数--监听页面初次渲染完成
	*/
	onReady: function () {

	},

	/**
	* 生命周期函数--监听页面显示
	*/
	onShow: function () {

	},

	/**
	* 生命周期函数--监听页面隐藏
	*/
	onHide: function () {

	},

	/**
	* 生命周期函数--监听页面卸载
	*/
	onUnload: function () {

	},

	/**
	* 页面相关事件处理函数--监听用户下拉动作
	*/
	onPullDownRefresh: function () {
		this.onLoad();
		wx.stopPullDownRefresh();
	},

	/**
	* 页面上拉触底事件的处理函数
	*/
	onReachBottom: function () {

	},

	/**
	* 用户点击右上角分享
	*/
	onShareAppMessage: function () {

	}
})