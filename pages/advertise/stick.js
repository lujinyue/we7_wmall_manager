//pages/advertise/stick.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		displayorder: 0,
		day: 0
	},

	/**
	* 生命周期函数--监听页面加载
	*/
	onLoad: function (options) {
		var that = this;
		app.util.request({
			url: 'manage/advertise/index/stick',
			data: {type: options.type || 'stick'},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				that.setData(result.message);
				if(options.type == 'stick') {
					for(var i in that.data.advertise.prices) {
						if(that.data.advertise.prices[i].sailed == 0) {
							for(var day in that.data.advertise.prices[i].fees) {
								that.data.day = day;
								break;
							}
							that.data.displayorder = that.data.advertise.prices[i].displayorder;
							break;
						}
					}
				} else {
					if(that.data.advertise.leave > 0) {
						for(var day in that.data.advertise.prices) {
							that.data.day = day;
							break;
						}
					}
				}
				that.setData({
					day: that.data.day,
					displayorder: that.data.displayorder
				});
				wx.setNavigationBarTitle({
					title: that.data.page_title
				});
			}
		});
	},

	onSelectPosition: function(e) {
		var that = this;
		var type = e.currentTarget.dataset.type;
		var index = e.currentTarget.dataset.index;
		if(that.data.type == 'stick') {
			if(type == 'day') {
				var priceSelect = that.data.advertise.prices[that.data.displayorder];
				if(priceSelect.sailed == 1) {
					return false;
				}
				that.setData({day: priceSelect.fees[index].day});
			} else {
				that.setData({displayorder: index});
			}
		} else {
			if(that.data.advertise.leave > 0) {
				that.setData({day: index});
			}
		}
	},

	onSubmit: function() {
		var that = this;
		if(that.data.type == 'stick' && !that.data.displayorder) {
			app.util.toast('请选择置顶位置', '', 1000);
			return false;
		}
		if(!that.data.day) {
			app.util.toast('请选择购买天数', '', 1000);
			return false;
		}
/*		if(!that.data.pay_type) {
			app.util.toast('请选择支付方式', '', 1000);
			return false;
		}*/
		var params = {
			type: that.data.type,
			displayorder: that.data.displayorder,
			day: that.data.day,
			pay_type: 'credit'
		};
		wx.showModal({
			title: '提示',
			content: '确定购买该推广活动吗?',
			success: function(res) {
				if(res.confirm) {
					app.util.request({
						url: 'manage/advertise/index/stick',
						data: params,
						method: 'POST',
						success: function (res) {
							var result = res.data.message;
							if(result.errno) {
								app.util.toast(result.message);
								return false;
							} else {
								wx.showToast({title: '下单成功'});
								app.util.pay({
									pay_type: 'credit',
									order_type: 'advertise',
									order_id: result.message.id,
									sid: result.message.sid,
								});
							}
						}
					});
				} else if(res.cancel) {

				}
			}
		});
	},

	/**
	* 生命周期函数--监听页面初次渲染完成
	*/
	onReady: function () {

	},

	/**
	* 生命周期函数--监听页面显示
	*/
	onShow: function () {

	},

	/**
	* 生命周期函数--监听页面隐藏
	*/
	onHide: function () {

	},

	/**
	* 生命周期函数--监听页面卸载
	*/
	onUnload: function () {

	},

	/**
	* 页面相关事件处理函数--监听用户下拉动作
	*/
	onPullDownRefresh: function () {

	},

	/**
	* 页面上拉触底事件的处理函数
	*/
	onReachBottom: function () {

	},

	/**
	* 用户点击右上角分享
	*/
	onShareAppMessage: function () {

	}
})