//pages/advertise/stick.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		type: 'recommendHome',
		day: {}
	},

	/**
	* 生命周期函数--监听页面加载
	*/
	onLoad: function () {
		var that = this;
		app.util.request({
			url: 'manage/advertise/index/recommend',
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				that.setData(result.message);
				if(that.data.advertise['recommendHome'].leave > 0) {
					for(var day in that.data.advertise['recommendHome'].prices) {
						that.data.day['recommendHome'] = day;
						break;
					}
				} else if(that.data.advertise['recommendOther'].leave > 0) {
					that.data.type = 'recommendOther';
					for(var day in that.data.advertise['recommendOther'].prices) {
						that.data.day['recommendOther'] = day;
						break;
					}
				}
				that.setData({
					day: that.data.day,
					type: that.data.type
				});
			}
		});
	},

	onSelectPosition: function(e) {
		var that = this;
		var type = e.currentTarget.dataset.type;
		var index = e.currentTarget.dataset.index;
		if(type == 'day') {
			var leave = that.data.advertise[that.data.type].leave;
			if(leave > 0) {
				that.data.day[that.data.type] = index;
				that.setData({day: that.data.day});
			}
		} else {
			that.setData({type: type});
		}

	},

	onSubmit: function() {
		var that = this;
		if(!that.data.type) {
			app.util.toast('请选择购买位置', '', 1000);
			return false;
		}
		if(!that.data.day[that.data.type]) {
			app.util.toast('请选择购买天数', '', 1000);
			return false;
		}
/*		if(!that.data.pay_type) {
			app.util.toast('请选择支付方式', '', 1000);
			return false;
		}*/
		var params = {
			type: that.data.type,
			day: that.data.day[that.data.type],
			pay_type: 'credit'
		};
		wx.showModal({
			title: '提示',
			content: '确定购买该推广活动吗?',
			success: function(res) {
				if(res.confirm) {
					app.util.request({
						url: 'manage/advertise/index/recommend',
						data: params,
						method: 'POST',
						success: function (res) {
							var result = res.data.message;
							if(result.errno) {
								app.util.toast(result.message);
								return false;
							} else {
								wx.showToast({title: '下单成功'});
								app.util.pay({
									pay_type: 'credit',
									order_type: 'advertise',
									order_id: result.message.id,
									sid: result.message.sid
								});
							}
						}
					});
				} else if(res.cancel) {

				}
			}
		});
	},

	/**
	* 生命周期函数--监听页面初次渲染完成
	*/
	onReady: function () {

	},

	/**
	* 生命周期函数--监听页面显示
	*/
	onShow: function () {

	},

	/**
	* 生命周期函数--监听页面隐藏
	*/
	onHide: function () {

	},

	/**
	* 生命周期函数--监听页面卸载
	*/
	onUnload: function () {

	},

	/**
	* 页面相关事件处理函数--监听用户下拉动作
	*/
	onPullDownRefresh: function () {

	},

	/**
	* 页面上拉触底事件的处理函数
	*/
	onReachBottom: function () {

	},

	/**
	* 用户点击右上角分享
	*/
	onShareAppMessage: function () {

	}
})