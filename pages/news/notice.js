//pages/news/notice.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		notice: {
			page: 1,
			psize: 10,
			loaded: false,
			empty: false,
			data: []
		}
	},

	onLoad: function (options) {
		var that = this;
		that.onPullDownRefresh();
		return;
	},

	onPullDownRefresh: function () {
		var that = this;
		that.setData({
			notice: {
				page: 1,
				psize: 10,
				loaded: false,
				empty: false,
				data: []
			}
		});
		that.onReachBottom();
		wx.stopPullDownRefresh();
	},

	onReachBottom: function (options) {
		var that = this;
		if (that.data.notice.loaded) {
			return false;
		}
		app.util.request({
			url: 'manage/news/notice/list',
			data: {
				page: that.data.notice.page,
				psize: that.data.notice.psize,
			},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				result = result.message;
				var notice = that.data.notice.data.concat(result.notice);
				that.data.notice.data = notice;
				that.data.notice.page++;
				if(!notice.length) {
					that.data.notice.empty = true;
				}
				if(result.notice.length < that.data.notice.psize) {
					that.data.notice.loaded = true;
				}
				that.setData({
					notice: that.data.notice
				})
			}
		})
	},

	onJsEvent: function(e) {
		app.util.jsEvent(e);
	},
})