var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		
	},
	onLoad: function (options) {
		var that = this;
		if(!options.id) {
			app.util.toast('参数错误');
			return false;
		}
		app.util.request({
			url: 'manage/news/notice/detail',
			data: {
				id: options.id
			},
			success: function(res) {
				var content = res.data.message.message.notice.content;
				app.WxParse.wxParse('content', 'html', content, that, 5);
				wx.setNavigationBarTitle({
					title: res.data.message.message.notice.title
				})
			}
		})
  	}
})