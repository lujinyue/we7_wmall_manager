// pages/tangshi/assignQueue.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		starttime: '00:00',
		endtime: '23:00'
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {

	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	},

	onSubmit: function (e) {
		var that = this;
		var formDate = e.detail.value;
		if (!formDate.title) {
			app.util.toast('队列名称不能为空', '', 1000);
			return false;
		}
		if (!formDate.guest_num) {
			app.util.toast('客人数量少于等于多少人排入此队列必须大于0', '', 1000);
			return false;
		}
		if (!formDate.prefix) {
			app.util.toast('队列编号前缀不能为空', '', 1000);
			return false;
		}
		if (!formDate.notify_num) {
			app.util.toast('提前通知人数必须大于0', '', 1000);
			return false;
		}
		formDate.starttime = that.data.starttime;
		formDate.endtime = that.data.endtime;
		app.util.request({
			url: 'manage/tangshi/assign/queue_post',
			data: {
				title: formDate.title,
				guest_num: formDate.guest_num,
				prefix: formDate.prefix,
				notify_num: formDate.notify_num,
				starttime: formDate.starttime,
				endtime: formDate.endtime,
				formid: e.detail.formId
			},
			method: 'POST',
			success: function (res) {
				var result = res.data.message;
				app.util.toast(result.message);
				if(result.errno) {
					return false;
				}
				app.util.toast('添加队列成功','./assign',1000);
			}
		});
	},

	onTimeChange: function(e) {
		var that = this;
		var index = e.currentTarget.dataset.index;
		var value = e.detail.value;
		that.data[index] = value;
		that.setData(that.data);
	},

})