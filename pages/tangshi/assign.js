// pages/tangshi/assign.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,

	},

	onLoad: function () {
		var that = this;
		app.util.request({
			url: 'manage/tangshi/assign/index',
			data: {},
			success: function (res) {
				var result = res.data.message;
				if (result.errno) {
					app.util.toast(result.message);
					return;
				}
				that.setData(result.message);
			}
		});		
	},

	onJsEvent: function (e) {
		app.util.jsEvent(e);
	},

	 onPullDownRefresh: function () {
	 	var that = this;
	 	that.onLoad();
		wx.stopPullDownRefresh();
	}
})