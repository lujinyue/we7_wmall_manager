// pages/tangshi/table.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		cid: 0
	},

	onLoad: function (options) {
		var that = this;
		that.data.options = options;
		app.util.request({
			url: 'manage/tangshi/table/index',
			data: {cid: options.cid || 0},
			success: function (res) {
				var result = res.data.message;
				if(result.errno) {
					app.util.toast(result.message);
					return false;
				}
				that.setData(result.message);
				if(that.data.options.cid) {
					that.setData({cid: that.data.options.cid});
				}
			}
		});
	},

	onChangeStatus: function(e) {
		var cid = e.currentTarget.dataset.cid;
		this.setData({cid: cid});
		this.onLoad({cid:cid});
	},

	onJsEvent: function (e) {
		app.util.jsEvent(e);
	},

	onPullDownRefresh: function () {
		this.onLoad({cid: this.data.cid});
		wx.stopPullDownRefresh();
	}
})