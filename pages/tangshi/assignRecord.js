// pages/tangshi/assignRecord.js
var app = getApp();
Page({
	data: {
		Lang: app.Lang,
		showLoading: false,
		records: {
			page: 1,
			psize: 15,
			empty: false,
			loaded: false,
			data: []
		}
	},

	onLoad: function(options) {
		var that = this;
		if (!options.id) {
			app.util.toast('参数错误');
			return false;
		}
		that.data.options = options;
		that.onReachBottom();
		return;
	},

	onPullDownRefresh: function () {
		var that = this;
		that.setData({
			records: {
				page: 1,
				psize: 15,
				empty: false,
				loaded: false,
				data: []
			}
		});
		that.onReachBottom();
		wx.stopPullDownRefresh();
	},

	onReachBottom: function () {
		var that = this;
		if(that.data.records.loaded) {
			return false;
		}
		that.setData({showLoading: true});
		app.util.request({
			url: 'manage/tangshi/assign/record',
			data: {
				id: that.data.options.id,
				page: that.data.records.page,
				psize: that.data.records.psize,
			},
			success: function(res) {
				var result = res.data.message;
				if (result.errno) {
					app.util.toast(result.message);
					return;
				}
				result = result.message;
				var records = that.data.records.data.concat(result.records);
				that.data.records.data = records;
				that.data.records.page++;
				if(!records.length) {
					that.data.records.empty = true;
				}
				if(result.records.length < that.data.records.psize) {
					that.data.records.loaded = true;
				}
				that.setData({
					records: that.data.records,
					showLoading: false,
					title: result.queue_title,
					id: that.data.options.id
				})
			}
		});
	},

	onJsEvent: function (e) {
		app.util.jsEvent(e);
	},
})